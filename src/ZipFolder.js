var fso = new ActiveXObject("Scripting.FileSystemObject");

var objArgs = WScript.Arguments;
var sourcePath = fso.GetAbsolutePathName(objArgs(0));
var zipPath = fso.GetAbsolutePathName(objArgs(1));

var file = fso.CreateTextFile(zipPath, true);
file.write("PK");
file.write(String.fromCharCode(5));
file.write(String.fromCharCode(6));
file.write('\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0');
file.close();

var objShell = new ActiveXObject("shell.application");
var zipFolder = objShell.NameSpace(zipPath);
var sourceItems = objShell.NameSpace(sourcePath).items();

zipFolder.CopyHere(sourceItems);
WScript.Sleep(1000);