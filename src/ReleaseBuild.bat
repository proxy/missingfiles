REM @echo off
SETLOCAL

SET OutputDirectory=..\release
SET ZipFileName=MissingFiles.zip

IF [%MsBuildPath%] == [] (
    SET MsBuildExe=c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe
) ELSE (
    SET MsBuildExe=%MsBuildPath%
)

"%MsBuildExe%" /nologo /t:MissingFiles:Rebuild /property:Configuration=Release

MKDIR "%OutputDirectory%"
DEL /F /Q "%OutputDirectory%\*"
COPY .\MissingFiles\bin\Release\MissingFiles.exe "%OutputDirectory%\"

DEL /F /Q %ZipFileName%
CScript ZipFolder.js "%OutputDirectory%\" %ZipFileName%
COPY MissingFiles.zip "%OutputDirectory%\%ZipFileName%"
DEL /F /Q %ZipFileName%
