﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using MissingFiles.Infrastructure;

namespace MissingFiles
{
    public partial class App : Application
    {

        // ---- Operations

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            UiThread.Initialize();

            if (!Debugger.IsAttached)
            {
                DispatcherUnhandledException += OnDispatcherUnhandledException;
                AppDomain.CurrentDomain.UnhandledException += OnAppDomainDomainUnhandledException;
            }
        }

        private void OnAppDomainDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
                return;

            var exception = (e.ExceptionObject as Exception);

            if (!UiThread.IsExecutingOnUiThread())
                UiThread.Invoke(() => { throw exception; });
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
                return;

            e.Handled = true;
            var exception = e.Exception;

            if (!UiThread.IsExecutingOnUiThread())
            {
                UiThread.Invoke(() => { throw exception; });
                return;
            }

            ReportException(exception);
            Shutdown();
        }

        private void ReportException(Exception exception)
        {
            const string ErrorMessage =
                "Oops, an unforeseen event has occured, sorry about that.\n" +
                "Sending an error report would be really helpful for the developer team. Do you want to send an error report?\n" +
                "(The error message is stored on the clipboard, please paste it to the e-mail body.)";

            var result = MessageBox.Show(ErrorMessage, "Error", MessageBoxButton.YesNo, MessageBoxImage.Error);
            if (result == MessageBoxResult.Yes)
                SendErrorReportEmail(exception);
        }

        private void SendErrorReportEmail(Exception exception)
        {
            const string MailToCommandLine = @"mailto:petruska.mark+MissingFiles@gmail.com?subject=MissingFiles%20error%20report";
            const string BodyFormat = "\n---- Please add details here\n\nException:\n{0}";

            string exceptionMessage = ((exception != null) ? exception.ToString() : "{Null}");
            string messageBody = string.Format(BodyFormat, exceptionMessage);

            Clipboard.SetText(messageBody);

            Process.Start(MailToCommandLine);
        }

    }
}
