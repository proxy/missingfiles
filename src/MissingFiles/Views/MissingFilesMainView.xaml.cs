﻿using MissingFiles.Controls;
using MissingFiles.ViewModels;

namespace MissingFiles.Views
{
    partial class MissingFilesMainView : BorderlessWindow
    {

        // ---- Fields

        private readonly MissingFilesMainViewModel _viewModel =
            new MissingFilesMainViewModel();

        // ---- Initialize

        public MissingFilesMainView()
        {
            InitializeComponent();

            DataContext = _viewModel;
        }

    }
}
