﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using MissingFiles.Controls;
using MissingFiles.ViewModels.Explore;

namespace MissingFiles.Views
{
    partial class DirectoryBundleExploreView : UserControl
    {

        // ---- Fields

        private readonly List<TargetDirectoryColumn> _directoryColumns =
            new List<TargetDirectoryColumn>();

        private readonly DispatcherTimer _filterTimer;

        // ---- Init

        public DirectoryBundleExploreView()
        {
            InitializeComponent();

            DataContextChanged += OnDataContextChanged;

            _filterTimer = CreateFilterTimer();
        }

        // ---- Properties

        private DirectoryBundleExploreViewModel ViewModel
        {
            get { return (DataContext as DirectoryBundleExploreViewModel); }
        }

        // ---- Operations

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs eventArgs)
        {
            var oldValue = (eventArgs.OldValue as DirectoryBundleExploreViewModel);
            UnObserveViewModel(oldValue);

            var newValue = (eventArgs.NewValue as DirectoryBundleExploreViewModel);
            ObserveViewModel(newValue);
        }

        private void ObserveViewModel(DirectoryBundleExploreViewModel viewModel)
        {
            if (viewModel == null)
                return;

            RebuildDirectoryColumns();
            viewModel.TargetDirectories.CollectionChanged += TargetDirectories_Changed;
        }

        private void UnObserveViewModel(DirectoryBundleExploreViewModel viewModel)
        {
            if (viewModel == null)
                return;

            viewModel.TargetDirectories.CollectionChanged -= TargetDirectories_Changed;
            ClearDirectoryColumns();
        }

        private void TargetDirectories_Changed(object sender, NotifyCollectionChangedEventArgs e)
        {
            RebuildDirectoryColumns();
        }

        private void RebuildDirectoryColumns()
        {
            ClearDirectoryColumns();
            CreateDirectoryColumns();
        }

        private void ClearDirectoryColumns()
        {
            foreach (var current in _directoryColumns)
                _entriesGrid.Columns.Remove(current);

            _directoryColumns.Clear();
        }

        private void CreateDirectoryColumns()
        {
            if ((ViewModel == null) || (ViewModel.TargetDirectories == null))
                return;

            for (int currentIndex = 0; currentIndex < ViewModel.TargetDirectories.Count; currentIndex++)
            {
                var current = ViewModel.TargetDirectories[currentIndex];
                var column = new TargetDirectoryColumn(currentIndex, current);
                _directoryColumns.Add(column);
                _entriesGrid.Columns.Add(column);
            }
        }

        private void Entries_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            var listViewItem = (e.Source as ListViewItem);
            if (listViewItem == null)
                return;

            var directoryEntry = (listViewItem.DataContext as DirectoryEntryViewModel);
            if (directoryEntry == null)
                return;

            var viewModel = ViewModel;
            if (viewModel == null)
                return;

            viewModel.DirectorySelected(directoryEntry);
        }

        private DispatcherTimer CreateFilterTimer()
        {
            const double DelaySeconds = 1;

            var filterTimer = new DispatcherTimer();
            filterTimer.Tick += FilterTimer_Tick;
            filterTimer.Interval = TimeSpan.FromSeconds(DelaySeconds);

            return filterTimer;
        }

        private void FilterTimer_Tick(object sender, System.EventArgs e)
        {
            _filterTimer.Stop();

            UpdateFilterProperty();
        }

        private void Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            _filterTimer.Stop();
            _filterTimer.Start();
        }

        private void Filter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            _filterTimer.Stop();

            UpdateFilterProperty();
        }

        private void UpdateFilterProperty()
        {
            var bindingExpression = _filterTextBox.GetBindingExpression(TextBox.TextProperty);
            bindingExpression.UpdateSource();
        }

    }
}
