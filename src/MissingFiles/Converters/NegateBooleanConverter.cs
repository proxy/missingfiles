﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MissingFiles.Converters
{
    internal class NegateBooleanConverter : IValueConverter
    {

        // ---- Instance

        public static NegateBooleanConverter Instance = new NegateBooleanConverter();

        // ---- Operations

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return false;

            bool boolValue = (bool)value;
            return !boolValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(value, targetType, parameter, culture);
        }

    }
}
