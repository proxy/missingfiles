﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace MissingFiles.Converters
{
    internal class BooleanToVisibilityConverter : IValueConverter, IMultiValueConverter
    {

        // ---- Properties

        public Visibility TrueValue { get; set; }

        public Visibility FalseValue { get; set; }

        // ---- Operations

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return FalseValue;

            bool isTrue = (bool)value;

            return (isTrue ? TrueValue : FalseValue);
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool anyIsTrue = values.OfType<bool>().Any(current => current);

            return (anyIsTrue ? TrueValue : FalseValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
