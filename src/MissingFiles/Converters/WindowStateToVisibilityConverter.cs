﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MissingFiles.Converters
{
    internal class WindowStateToVisibilityConverter : IValueConverter
    {

        // ---- Properties

        public Visibility NormalValue { get; set; }

        public Visibility MinimizedValue { get; set; }

        public Visibility MaximizedValue { get; set; }

        // ---- Operations

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is WindowState))
                return NormalValue;

            var windowState = (WindowState)value;
            switch (windowState)
            {
                case WindowState.Minimized:
                    return MinimizedValue;

                case WindowState.Maximized:
                    return MaximizedValue;

                default:
                    return NormalValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
