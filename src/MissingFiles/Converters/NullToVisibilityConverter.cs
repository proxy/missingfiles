﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace MissingFiles.Converters
{
    internal class NullToVisibilityConverter : IValueConverter, IMultiValueConverter
    {

        // ---- Properties

        public Visibility NullValue { get; set; }

        public Visibility NonNullValue { get; set; }

        // ---- Operations

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((value != null) ? NonNullValue : NullValue);
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool anyIsNonNull = values.Any(current => (current != null));
            return (anyIsNonNull ? NonNullValue : NullValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
