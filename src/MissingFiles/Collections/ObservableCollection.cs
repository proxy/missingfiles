﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using MissingFiles.Infrastructure;

namespace MissingFiles.Collections
{
    internal class ObservableCollection<T> : System.Collections.ObjectModel.ObservableCollection<T>
    {

        // ---- Fields

        private static readonly string CountPropertyName = "Count";
        private static readonly string ItemsPropertyName = System.Windows.Data.Binding.IndexerName;

        // ---- Init

        public ObservableCollection()
            : base()
        {
        }

        public ObservableCollection(IEnumerable<T> collection)
            : base(collection)
        {
        }

        public ObservableCollection(List<T> list)
            : base(list)
        {
        }

        // ---- Operations

        public void AddRange(IEnumerable<T> items)
        {
            Argument.ShouldNotBeNull(items, "items");

            CheckReentrancy();

            foreach (var current in items)
                Items.Add(current);

            OnPropertyChanged(CountPropertyName);
            OnPropertyChanged(ItemsPropertyName);
            OnCollectionReset();
        }

        public bool MoveItemUp(T item)
        {
            int index = IndexOf(item);
            if ((index < 1) || (index >= Count))
                return false;

            int newIndex = index - 1;

            Remove(item);
            Insert(newIndex, item);

            return true;
        }

        public bool MoveItemDown(T item)
        {
            int index = IndexOf(item);
            if ((index < 0) || (index >= Count - 1))
                return false;

            int newIndex = index + 1;

            Remove(item);

            if (newIndex < Count)
                Insert(newIndex, item);
            else
                Add(item);

            return true;
        }

        private void OnPropertyChanged(string propertyName)
        {
            var eventArgs = new PropertyChangedEventArgs(propertyName);
            OnPropertyChanged(eventArgs);
        }

        private void OnCollectionReset()
        {
            var eventArgs = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            OnCollectionChanged(eventArgs);
        }

    }
}
