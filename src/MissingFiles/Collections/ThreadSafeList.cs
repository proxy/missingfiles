﻿using System.Collections.Generic;
using MissingFiles.Infrastructure;

namespace MissingFiles.Collections
{
    internal class ThreadSafeList<T>
    {

        // ---- Fields

        private readonly object _itemsGuard = new object();
        private readonly List<T> _items = new List<T>();

        // ---- Operations

        public void Add(T item)
        {
            lock (_itemsGuard)
            {
                _items.Add(item);
            }
        }

        public void AddRange(IEnumerable<T> collection)
        {
            Argument.ShouldNotBeNull(collection, "collection");

            lock (_itemsGuard)
            {
                _items.AddRange(collection);
            }
        }

        public void Clear()
        {
            lock (_itemsGuard)
            {
                _items.Clear();
            }
        }

        public T[] ToArray()
        {
            lock (_itemsGuard)
            {
                var result = _items.ToArray();
                return result;
            }
        }

    }
}
