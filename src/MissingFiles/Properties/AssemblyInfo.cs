﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyTitle("MissingFiles")]
[assembly: AssemblyDescription("Multi-directory explorer")]
[assembly: AssemblyCompany("Mark Petruska")]
[assembly: AssemblyProduct("MissingFiles")]
[assembly: AssemblyCopyright("Copyright © 2013 Mark Petruska")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: ThemeInfo(
    ResourceDictionaryLocation.None,
    ResourceDictionaryLocation.SourceAssembly
)]

[assembly: AssemblyVersion("1.2.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]


#if DEBUG
    [assembly: AssemblyConfiguration("DEBUG")]
#else
    [assembly: AssemblyConfiguration("RELEASE")]
#endif