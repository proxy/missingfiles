﻿using MissingFiles.Infrastructure;

namespace MissingFiles.Filters.Glob
{
    internal class GlobFilter
    {

        // ---- Fields

        private readonly string _pattern;

        // ---- Init

        public GlobFilter(string pattern)
        {
            _pattern = pattern;
        }

        // ---- Properties

        public bool IsFiltering
        {
            get { return (!string.IsNullOrEmpty(_pattern)); }
        }

        // ---- Operations

        public bool Passes(string input)
        {
            if (string.IsNullOrEmpty(_pattern))
                return true;

            if (string.IsNullOrEmpty(input))
                return false;

            bool passes = Passes(0, input, 0);
            return passes;
        }

        private bool Passes(int patternIndex, string input, int inputIndex)
        {
            Argument.ShouldNotBeNull(input, "input");
            Argument.CheckRange(patternIndex >= 0, "patternIndex");
            Argument.CheckRange(inputIndex >= 0, "patternIndex");

            const char AnyCharacter = '?';
            const char AnyCharacterAnyTimes = '*';

            if ((patternIndex >= _pattern.Length) && (inputIndex >= input.Length))
                return true;

            if (patternIndex >= _pattern.Length)
                return false;

            char patternCharacter = _pattern[patternIndex];
            if (patternCharacter == AnyCharacterAnyTimes)
            {
                patternIndex++;
                if ((patternIndex >= _pattern.Length) && (inputIndex >= input.Length))
                    return true;

                for (int testIndex = input.Length; testIndex >= inputIndex; testIndex--)
                {
                    bool passes = Passes(patternIndex, input, testIndex);
                    if (passes)
                        return true;
                }

                return false;
            }
            else
            {
                if (inputIndex >= input.Length)
                    return false;

                if ((patternCharacter != AnyCharacter) && (char.ToLowerInvariant(patternCharacter) != char.ToLowerInvariant(input[inputIndex])))
                    return false;

                patternIndex++;
                inputIndex++;

                bool passes = Passes(patternIndex, input, inputIndex);
                return passes;
            }
        }

    }
}
