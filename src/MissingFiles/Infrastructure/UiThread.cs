﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace MissingFiles.Infrastructure
{
    internal static class UiThread
    {

        // ---- Fields

        private static Dispatcher _uiDispatcher;

        // ---- Operations

        public static void Initialize()
        {
            _uiDispatcher = Dispatcher.CurrentDispatcher;
        }

        [Conditional("DEBUG")]
        public static void CheckCrossThreadAccess()
        {
            if (!IsExecutingOnUiThread())
                throw new UnauthorizedAccessException("Invalid cross-thread access.");
        }

        public static bool IsExecutingOnUiThread()
        {
            return (Dispatcher.CurrentDispatcher == _uiDispatcher);
        }

        public static void Invoke(Action action)
        {
            Argument.ShouldNotBeNull(action, "action");

            if (Dispatcher.CurrentDispatcher != _uiDispatcher)
                _uiDispatcher.Invoke(action);
            else
                action();
        }

        public static void BeginInvoke(Action action)
        {
            Argument.ShouldNotBeNull(action, "action");

            _uiDispatcher.BeginInvoke(action);
        }

    }
}
