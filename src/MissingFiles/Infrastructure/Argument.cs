﻿using System;
using System.Diagnostics;

namespace MissingFiles.Infrastructure
{
    internal static class Argument
    {

        // ---- Operations

        [Conditional("DEBUG")]
        public static void ShouldNotBeNull(object actualValue, string parameterName)
        {
            if (actualValue == null)
                throw new ArgumentNullException(parameterName);
        }

        [Conditional("DEBUG")]
        public static void Check(bool checkValue, string parameterName, string message)
        {
            if (!checkValue)
                throw new ArgumentException(message, parameterName);
        }

        [Conditional("DEBUG")]
        public static void CheckRange(bool checkValue, string parameterName)
        {
            if (!checkValue)
                throw new ArgumentOutOfRangeException(parameterName);
        }

    }
}
