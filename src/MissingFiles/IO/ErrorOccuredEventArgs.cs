﻿using System;
using MissingFiles.Infrastructure;

namespace MissingFiles.IO
{
    public sealed class ErrorOccuredEventArgs : EventArgs
    {

        // ---- Fields

        private readonly string _errorMessage;

        // ---- Init

        public ErrorOccuredEventArgs(string errorMessage)
        {
            Argument.ShouldNotBeNull(errorMessage, "errorMessage");

            _errorMessage = errorMessage;
        }

        // ---- Properties

        public string ErrorMessage
        {
            get { return _errorMessage; }
        }

    }
}
