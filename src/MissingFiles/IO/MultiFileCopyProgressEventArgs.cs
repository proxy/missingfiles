﻿using System;
using MissingFiles.Infrastructure;

namespace MissingFiles.IO
{
    public sealed class MultiFileCopyProgressEventArgs : EventArgs
    {

        // ---- Fields

        private readonly long _totalFilesCount;
        private readonly long _finishedFilesCount;
        private readonly FileCopyProgressEventArgs _currentFileProgress;

        // ---- Init

        public MultiFileCopyProgressEventArgs(long totalFilesCount, long finishedFilesCount, FileCopyProgressEventArgs currentFileProgress)
        {
            Argument.ShouldNotBeNull(currentFileProgress, "currentFileProgress");

            _totalFilesCount = totalFilesCount;
            _finishedFilesCount = finishedFilesCount;
            _currentFileProgress = currentFileProgress;
        }

        // ---- Properties

        public long TotalFilesCount
        {
            get { return _totalFilesCount; }
        }

        public long FinishedFilesCount
        {
            get { return _finishedFilesCount; }
        }

        public FileCopyProgressEventArgs CurrentFileProgress
        {
            get { return _currentFileProgress; }
        }

        public double Progress
        {
            get
            {
                if (_totalFilesCount == 0L)
                    return 0.0;

                return ((double)_finishedFilesCount / (double)_totalFilesCount) * 100.0;
            }
        }

    }
}
