﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using MissingFiles.Infrastructure;

namespace MissingFiles.IO
{
    public sealed class ObservableDirectoryCopy
    {

        // ---- Fields

        private readonly string[] _directoryList;
        private readonly ObservableFileCopy[] _fileCopyList;

        private readonly object _currentFileCopyGuard = new object();
        private ObservableFileCopy _currentFileCopy;

        private readonly long _totalCount;
        private long _finishedCount;

        private volatile bool _isCancelled = false;

        // ---- Init

        public ObservableDirectoryCopy(IEnumerable<string> directoriesToCreate, IEnumerable<ObservableFileCopy> copyList)
        {
            Argument.ShouldNotBeNull(copyList, "copyList");

            _fileCopyList = copyList.ToArray();
            _totalCount = _fileCopyList.LongLength;

            if (directoriesToCreate != null)
                _directoryList = directoriesToCreate.ToArray();
            else
                _directoryList = new string[0];
        }

        // ---- Events

        public event EventHandler<MultiFileCopyProgressEventArgs> ProgressChanged;

        public event EventHandler<ErrorOccuredEventArgs> ErrorOccured;

        // ---- Operations

        public void Execute()
        {
            Interlocked.Exchange(ref _finishedCount, 0L);
            _isCancelled = false;

            foreach (var currentDirectoryPath in _directoryList)
            {
                if (!Directory.Exists(currentDirectoryPath))
                    Directory.CreateDirectory(currentDirectoryPath);
            }

            foreach (var currentFileCopy in _fileCopyList)
            {
                if (_isCancelled)
                    return;

                currentFileCopy.ProgressChanged += FileCopy_ProgressChanged;

                lock (_currentFileCopyGuard)
                    _currentFileCopy = currentFileCopy;

                TryExecuteFileCopy(currentFileCopy);

                currentFileCopy.ProgressChanged -= FileCopy_ProgressChanged;

                lock (_currentFileCopyGuard)
                    _currentFileCopy = null;

                Interlocked.Increment(ref _finishedCount);
            }
        }

        public void Cancel()
        {
            _isCancelled = true;

            lock (_currentFileCopyGuard)
            {
                if (_currentFileCopy != null)
                    _currentFileCopy.Cancel();
            }
        }

        private void OnProgressChanged(MultiFileCopyProgressEventArgs eventArgs)
        {
            Argument.ShouldNotBeNull(eventArgs, "eventArgs");

            var handler = ProgressChanged;

            if (handler != null)
                handler(this, eventArgs);
        }

        private void OnErrorOccured(ErrorOccuredEventArgs eventArgs)
        {
            Argument.ShouldNotBeNull(eventArgs, "eventArgs");

            var handler = ErrorOccured;

            if (handler != null)
                handler(this, eventArgs);
        }

        private void TryExecuteFileCopy(ObservableFileCopy fileCopy)
        {
            const string ErrorMessageFormat = "{0}: {1}";

            try
            {
                fileCopy.Execute();
            }
            catch (Exception ex)
            {
                string errorMessage = string.Format(ErrorMessageFormat, fileCopy.DestinationFilePath, ex.Message);
                ReportErrorOccured(errorMessage);
            }
        }

        private void FileCopy_ProgressChanged(object sender, FileCopyProgressEventArgs eventArgs)
        {
            long finishedFilesCount = Interlocked.Read(ref _finishedCount);
            ReportProgressChanged(_totalCount, finishedFilesCount, eventArgs);
        }

        private void ReportProgressChanged(long totalFilesCount, long finishedFilesCount, FileCopyProgressEventArgs currentFileProgress)
        {
            var eventArgs = new MultiFileCopyProgressEventArgs(totalFilesCount, finishedFilesCount, currentFileProgress);
            OnProgressChanged(eventArgs);
        }

        private void ReportErrorOccured(string errorMessage)
        {
            Argument.ShouldNotBeNull(errorMessage, "errorMessage");

            var eventArgs = new ErrorOccuredEventArgs(errorMessage);
            OnErrorOccured(eventArgs);
        }

    }
}
