﻿using System;
using MissingFiles.Infrastructure;

namespace MissingFiles.IO
{
    public sealed class FileCopyProgressEventArgs : EventArgs
    {

        // ---- Fields

        private static readonly long BytesInMegaByte = 1024L * 1024L;

        private readonly string _sourceFilePath;
        private readonly string _destinationFilePath;
        private readonly long _totalBytes;
        private readonly long _transferredBytes;

        // ---- Init

        public FileCopyProgressEventArgs(string sourceFilePath, string destinationFilePath, long totalBytes, long transferredBytes)
        {
            Argument.ShouldNotBeNull(sourceFilePath, "sourceFilePath");
            Argument.ShouldNotBeNull(destinationFilePath, "destinationFilePath");

            _sourceFilePath = sourceFilePath;
            _destinationFilePath = destinationFilePath;
            _totalBytes = totalBytes;
            _transferredBytes = transferredBytes;
        }

        // ---- Properties

        public string SourceFilePath
        {
            get { return _sourceFilePath; }
        }

        public string DestinationFilePath
        {
            get { return _destinationFilePath; }
        }

        public long TotalBytes
        {
            get { return _totalBytes; }
        }

        public long TransferredBytes
        {
            get { return _transferredBytes; }
        }

        public double TotalMegaBytes
        {
            get { return ((double)_totalBytes / (double)BytesInMegaByte); }
        }

        public double TransferredMegaBytes
        {
            get { return ((double)_transferredBytes / (double)BytesInMegaByte); }
        }

        public double Progress
        {
            get
            {
                if (_totalBytes == 0L)
                    return 0.0;

                return ((double)_transferredBytes / (double)_totalBytes) * 100.0;
            }
        }

    }
}
