﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using MissingFiles.Infrastructure;

namespace MissingFiles.IO
{
    public sealed class ObservableFileCopy
    {

        // ---- External

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CopyFileEx(string lpExistingFileName, string lpNewFileName, CopyProgressRoutine lpProgressRoutine, IntPtr lpData,
            ref Int32 pbCancel, CopyFileFlags dwCopyFlags);

        private delegate CopyProgressResult CopyProgressRoutine(long TotalFileSize, long TotalBytesTransferred, long StreamSize, long StreamBytesTransferred, uint dwStreamNumber,
            CopyProgressCallbackReason dwCallbackReason, IntPtr hSourceFile, IntPtr hDestinationFile, IntPtr lpData);

        private enum CopyProgressResult : uint
        {
            PROGRESS_CONTINUE = 0,
            PROGRESS_CANCEL = 1,
            PROGRESS_STOP = 2,
            PROGRESS_QUIET = 3
        }

        private enum CopyProgressCallbackReason : uint
        {
            CALLBACK_CHUNK_FINISHED = 0x00000000,
            CALLBACK_STREAM_SWITCH = 0x00000001
        }

        [Flags]
        private enum CopyFileFlags : uint
        {
            COPY_FILE_FAIL_IF_EXISTS = 0x00000001,
            COPY_FILE_RESTARTABLE = 0x00000002,
            COPY_FILE_OPEN_SOURCE_FOR_WRITE = 0x00000004,
            COPY_FILE_ALLOW_DECRYPTED_DESTINATION = 0x00000008
        }

        // ---- Fields

        private static readonly string ExtendedFilePathFormat = @"\\?\{0}";
        private static readonly string ExtendedUncFilePathFormat = @"\\?\UNC\{0}";

        private readonly string _sourceFilePath;
        private readonly string _destinationFilePath;

        private const int True = 1;
        private int _pbCancel;

        // ---- Init

        public ObservableFileCopy(string sourceFilePath, string destinationFilePath)
        {
            Argument.ShouldNotBeNull(sourceFilePath, "sourceFilePath");
            Argument.ShouldNotBeNull(destinationFilePath, "destinationFilePath");

            _sourceFilePath = sourceFilePath;
            _destinationFilePath = destinationFilePath;
        }

        // ---- Properties

        public string SourceFilePath
        {
            get { return _sourceFilePath; }
        }

        public string DestinationFilePath
        {
            get { return _destinationFilePath; }
        }

        // ---- Events

        public event EventHandler<FileCopyProgressEventArgs> ProgressChanged;

        // ---- Operations

        public void Execute()
        {
            if (_pbCancel == True)
                return;

            string extendedSourceFilePath = GetExtendedFilePath(_sourceFilePath);
            string extendedDestinationFilePath = GetExtendedFilePath(_destinationFilePath);

            string destinationDirectory = Path.GetDirectoryName(_destinationFilePath);
            if (!Directory.Exists(destinationDirectory))
                Directory.CreateDirectory(destinationDirectory);

            CopyFileEx(extendedSourceFilePath, extendedDestinationFilePath, CopyProgress, IntPtr.Zero, ref _pbCancel, CopyFileFlags.COPY_FILE_FAIL_IF_EXISTS | CopyFileFlags.COPY_FILE_ALLOW_DECRYPTED_DESTINATION);

            int lastError = Marshal.GetLastWin32Error();
            if (lastError != 0)
                throw new Win32Exception(lastError);
        }

        public void Cancel()
        {
            _pbCancel = True;
        }

        private void OnProgressChanged(FileCopyProgressEventArgs eventArgs)
        {
            var handler = ProgressChanged;

            if (handler != null)
                handler(this, eventArgs);
        }

        private string GetExtendedFilePath(string filePath)
        {
            const string SharePrefix = @"\\";

            Argument.ShouldNotBeNull(filePath, "filePath");

            if (filePath.StartsWith(SharePrefix))
                return string.Format(ExtendedUncFilePathFormat, filePath.Substring(SharePrefix.Length));
            else
                return string.Format(ExtendedFilePathFormat, filePath);
        }

        private CopyProgressResult CopyProgress(long TotalFileSize, long TotalBytesTransferred, long StreamSize, long StreamBytesTransferred, uint dwStreamNumber,
            CopyProgressCallbackReason dwCallbackReason, IntPtr hSourceFile, IntPtr hDestinationFile, IntPtr lpData)
        {
            ReportProgressChanged(TotalFileSize, TotalBytesTransferred);

            if (_pbCancel != True)
                return CopyProgressResult.PROGRESS_CONTINUE;
            else
                return CopyProgressResult.PROGRESS_CANCEL;
        }

        private void ReportProgressChanged(long totalBytes, long transferredBytes)
        {
            var eventArgs = new FileCopyProgressEventArgs(_sourceFilePath, _destinationFilePath, totalBytes, transferredBytes);
            OnProgressChanged(eventArgs);
        }

    }
}
