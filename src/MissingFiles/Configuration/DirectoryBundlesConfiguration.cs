﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace MissingFiles.Configuration
{
    [XmlRoot(ElementName = "DirectoryBundles", Namespace = "")]
    public sealed class DirectoryBundlesConfiguration
    {

        // ---- Fields

        public static readonly string DefaultPath = "directoryBundles.xml";

        private static readonly XmlSerializer _serializer =
            new XmlSerializer(typeof(DirectoryBundlesConfiguration), "");

        // ---- Properties

        [XmlElement("DirectoryBundle", Namespace = "", Order = 1)]
        public List<DirectoryBundleConfiguration> Items { get; set; }

        // ---- Operations

        public static DirectoryBundlesConfiguration CreateNew()
        {
            var result =
                new DirectoryBundlesConfiguration
                {
                    Items = new List<DirectoryBundleConfiguration>()
                };

            return result;
        }

        public static DirectoryBundlesConfiguration Load()
        {
            var result = LoadFrom(DefaultPath);
            if (result == null)
                return CreateNew();

            return result;
        }

        public static DirectoryBundlesConfiguration LoadFrom(string filePath)
        {
            if (!File.Exists(filePath))
                return null;

            try
            {
                var result = DeserializeFrom(filePath);
                return result;
            }
            catch
            {
                TryRenameFileToUnreadable(filePath);
                return null;
            }
        }

        public void Save()
        {
            SaveTo(DefaultPath);
        }

        public void SaveTo(string filePath)
        {
            using (var stream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                _serializer.Serialize(stream, this);
            }
        }

        private static DirectoryBundlesConfiguration DeserializeFrom(string filePath)
        {
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var result = (_serializer.Deserialize(stream) as DirectoryBundlesConfiguration);
                return result;
            }
        }

        private static void TryRenameFileToUnreadable(string filePath)
        {
            const string UnreadableFileNameFormat = "{0}.unreadable";

            try
            {
                string directory = Path.GetDirectoryName(filePath);
                string originalFileName = Path.GetFileName(filePath);
                string newFileName = string.Format(UnreadableFileNameFormat, originalFileName);
                string newFilePath = Path.Combine(directory, newFileName);

                File.Move(filePath, newFilePath);
            }
            catch
            {
                // Best effort.
            }
        }

    }
}
