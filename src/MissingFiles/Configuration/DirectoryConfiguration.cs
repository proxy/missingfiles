﻿using System.Xml.Serialization;

namespace MissingFiles.Configuration
{
    public sealed class DirectoryConfiguration
    {

        // ---- Properties

        [XmlAttribute("Name", Namespace = "")]
        public string Name { get; set; }

        [XmlText]
        public string Path { get; set; }

        // ---- Operations

        public static DirectoryConfiguration CreateNew()
        {
            return
                new DirectoryConfiguration
                {
                    Name = string.Empty,
                    Path = string.Empty,
                };
        }

    }
}
