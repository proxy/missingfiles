﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MissingFiles.Configuration
{
    public sealed class DirectoryBundleConfiguration
    {

        // ---- Properties

        [XmlAttribute("Name", Namespace = "")]
        public string Name { get; set; }

        [XmlElement("Directory", Namespace = "", Order = 1)]
        public List<DirectoryConfiguration> Directories { get; set; }

        // ---- Operations

        public static DirectoryBundleConfiguration CreateNew()
        {
            var instance =
                new DirectoryBundleConfiguration
                {
                    Name = string.Empty,
                    Directories = new List<DirectoryConfiguration>(),
                };

            return instance;
        }

    }
}
