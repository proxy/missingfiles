﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MissingFiles.Collections;
using MissingFiles.Infrastructure;

namespace MissingFiles.Binding
{
    internal class BackgroundOperation : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly CancellationTokenSource _cancellationTokenSource =
            new CancellationTokenSource();

        private readonly Action<BackgroundOperation, CancellationToken> _execute;
        private readonly Command _cancelCommand;
        private readonly Command _clearErrorCommand;
        private readonly bool _canCancel;

        private bool _isVisible = false;
        private bool _isRunning = false;
        private bool _currentRunCancelled;

        private bool _canReportProgress = false;
        private double _currentProgress = 0.0;
        private string _statusText = string.Empty;

        private bool _hasSubOperation = false;
        private double _subOperationProgress = 0.0;
        private string _subOperationStatusText = string.Empty;

        private readonly ThreadSafeList<string> _errors = new ThreadSafeList<string>();
        private string _errorMessage;

        // ---- Init

        public BackgroundOperation(Action<BackgroundOperation, CancellationToken> execute, bool canCancel)
        {
            Argument.ShouldNotBeNull(execute, "execute");

            _execute = execute;
            _canCancel = canCancel;

            _cancelCommand = new Command(_ => Cancel(), _ => (_canCancel && _isRunning && !_currentRunCancelled));
            _clearErrorCommand = new Command(_ => ClearErrors(), null);
        }

        // ---- Properties

        public bool IsVisible
        {
            get { return _isVisible; }
            private set
            {
                if (_isVisible == value)
                    return;

                _isVisible = value;

                ReportPropertyChanged("IsVisible");
            }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            private set
            {
                UiThread.CheckCrossThreadAccess();

                if (_isRunning == value)
                    return;

                _isRunning = value;

                ReportPropertyChanged("IsRunning");

                UpdateIsVisible();
                _cancelCommand.ReportCanExecuteChanged();
            }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            private set
            {
                if (_errorMessage == value)
                    return;

                _errorMessage = value;

                ReportPropertyChanged("ErrorMessage");

                UpdateIsVisible();
            }
        }

        public bool CanReportProgress
        {
            get { return _canReportProgress; }
            set
            {
                if (_canReportProgress == value)
                    return;

                _canReportProgress = value;

                ReportPropertyChanged("CanReportProgress");
            }
        }

        public double CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                if (_currentProgress == value)
                    return;

                _currentProgress = value;

                ReportPropertyChanged("CurrentProgress");
            }
        }

        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (_statusText == value)
                    return;

                _statusText = value;

                ReportPropertyChanged("StatusText");
            }
        }

        public bool HasSubOperation
        {
            get { return _hasSubOperation; }
            set
            {
                if (_hasSubOperation == value)
                    return;

                _hasSubOperation = value;

                ReportPropertyChanged("HasSubOperation");
            }
        }

        public double SubOperationProgress
        {
            get { return _subOperationProgress; }
            set
            {
                if (_subOperationProgress == value)
                    return;

                _subOperationProgress = value;

                ReportPropertyChanged("SubOperationProgress");
            }
        }

        public string SubOperationStatusText
        {
            get { return _subOperationStatusText; }
            set
            {
                if (_subOperationStatusText == value)
                    return;

                _subOperationStatusText = value;

                ReportPropertyChanged("SubOperationStatusText");
            }
        }

        public Command CancelCommand
        {
            get { return _cancelCommand; }
        }

        public Command ClearErrorCommand
        {
            get { return _clearErrorCommand; }
        }

        // ---- Events

        public event EventHandler FinishedAndClear;

        // ---- Operations

        public void Execute()
        {
            UiThread.Invoke(
                () =>
                {
                    _currentRunCancelled = false;
                    IsRunning = true;

                    Task.Factory.StartNew(() => _execute(this, _cancellationTokenSource.Token), _cancellationTokenSource.Token).
                        ContinueWith(executeTask => UiThread.Invoke(() => ExecutionFinished(executeTask)));
                });
        }

        public void Cancel()
        {
            UiThread.Invoke(
                () =>
                {
                    _cancellationTokenSource.Cancel();
                    _currentRunCancelled = true;
                    _cancelCommand.ReportCanExecuteChanged();
                });
        }

        public void ClearErrors()
        {
            _errors.Clear();

            UiThread.Invoke(
                () =>
                {
                    ErrorMessage = null;
                    ReportFinishedAndClear();
                });
        }

        public void AddError(string errorMessage)
        {
            _errors.Add(errorMessage);

            UiThread.Invoke(
                () =>
                {
                    FormatErrorMessage();
                });
        }

        public void AddErrors(IEnumerable<string> errorMessages)
        {
            if (errorMessages == null)
                return;

            _errors.AddRange(errorMessages);

            UiThread.Invoke(
                () =>
                {
                    FormatErrorMessage();
                });
        }

        protected virtual void OnFinishedAndClear(EventArgs eventArgs)
        {
            var handler = FinishedAndClear;

            if (handler != null)
                handler(this, eventArgs);
        }

        private void UpdateIsVisible()
        {
            UiThread.CheckCrossThreadAccess();

            IsVisible = ((_isRunning) || (!string.IsNullOrEmpty(_errorMessage)));
        }

        private void ReportFinishedAndClear()
        {
            UiThread.Invoke(
                () =>
                {
                    var eventArgs = EventArgs.Empty;
                    OnFinishedAndClear(eventArgs);
                });
        }

        private void ExecutionFinished(Task executeTask)
        {
            UiThread.CheckCrossThreadAccess();

            _currentRunCancelled = false;
            IsRunning = false;
            CurrentProgress = 100.0;
            CanReportProgress = true;

            CollectErrorMessageFrom(executeTask.Exception);

            if (string.IsNullOrEmpty(_errorMessage))
                ReportFinishedAndClear();
        }

        private void CollectErrorMessageFrom(AggregateException aggregateException)
        {
            UiThread.CheckCrossThreadAccess();

            if (aggregateException == null)
                return;

            var exceptionsToProcess = new List<Exception>();
            AddException(exceptionsToProcess, aggregateException);

            var exceptionMessages =
                (from currentException in exceptionsToProcess
                 where (currentException != null) && (!(currentException is OperationCanceledException)) &&
                       (currentException.Message != null)
                 select currentException.Message);

            _errors.AddRange(exceptionMessages);
            FormatErrorMessage();
        }

        private void AddException(List<Exception> exceptionsList, Exception exception)
        {
            var aggregateException = (exception as AggregateException);

            if (aggregateException != null)
            {
                if (aggregateException.InnerExceptions != null)
                    AddExceptions(exceptionsList, aggregateException.InnerExceptions);
                else if (aggregateException.InnerException != null)
                    AddException(exceptionsList, aggregateException.InnerException); 
            }
            else if (exception != null)
            {
                exceptionsList.Add(exception);
            }
        }

        private void AddExceptions(List<Exception> exceptionsList, IEnumerable<Exception> exceptions)
        {
            if (exceptions == null)
                return;

            foreach (var current in exceptions)
                AddException(exceptionsList, current);
        }

        private void FormatErrorMessage()
        {
            const string ErrorMessageFormat = "The following errors occured:\n{0}";
            const string ErrorMessageListFormat = " - {0}";
            const string ErrorMessageListSeparator = "\n";

            UiThread.CheckCrossThreadAccess();

            var errors = _errors.ToArray();

            if (errors.Length <= 0)
            {
                ErrorMessage = null;
                return;
            }

            if (errors.Length == 1)
            {
                ErrorMessage = errors.FirstOrDefault();
                return;
            }

            var listErrorMessages = errors.Select(current => string.Format(ErrorMessageListFormat, current));
            string errorMessagesList = string.Join(ErrorMessageListSeparator, listErrorMessages);
            string errorMessage = string.Format(ErrorMessageFormat, errorMessagesList);

            ErrorMessage = errorMessage;
        }

    }
}
