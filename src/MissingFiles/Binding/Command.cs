﻿using System;
using System.Windows.Input;
using MissingFiles.Infrastructure;

namespace MissingFiles.Binding
{
    internal class Command : ICommand
    {

        // ---- Fields

        private readonly Action<object> _execute;
        private readonly Func<object, bool> _canExecute;

        // ---- Init

        public Command(Action<object> execute, Func<object, bool> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        // ---- Events

        public event EventHandler CanExecuteChanged;

        // ---- Operations

        public bool CanExecute(object parameter)
        {
            if (_execute == null)
                return false;

            if (_canExecute == null)
                return true;

            bool canExecute = _canExecute(parameter);
            return canExecute;
        }

        public void Execute(object parameter)
        {
            if (_execute == null)
                return;

            _execute(parameter);
        }

        public void ReportCanExecuteChanged()
        {
            var eventArgs = EventArgs.Empty;
            OnCanExecuteChanged(eventArgs);
        }

        protected virtual void OnCanExecuteChanged(EventArgs eventArgs)
        {
            UiThread.CheckCrossThreadAccess();

            var handler = CanExecuteChanged;

            if (handler != null)
                handler(this, eventArgs);
        }

    }
}
