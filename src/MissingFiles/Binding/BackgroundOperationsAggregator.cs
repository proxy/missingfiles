﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MissingFiles.Infrastructure;

namespace MissingFiles.Binding
{
    internal class BackgroundOperationsAggregator : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly object _backgroundOperationsGuard = new object();
        private readonly List<BackgroundOperation> _backgroundOperations =
            new List<BackgroundOperation>();

        private BackgroundOperation _currentOperation;

        // ---- Init

        public BackgroundOperationsAggregator(params BackgroundOperation[] backgroundOperations)
            : this((IEnumerable<BackgroundOperation>)backgroundOperations)
        {
        }

        public BackgroundOperationsAggregator(IEnumerable<BackgroundOperation> backgroundOperations)
        {
            if (backgroundOperations != null)
                AddRange(backgroundOperations);
        }

        // ---- Properties

        public BackgroundOperation CurrentOperation
        {
            get { return _currentOperation; }
            private set
            {
                if (_currentOperation == value)
                    return;

                _currentOperation = value;

                ReportPropertyChanged("CurrentOperation");
            }
        }

        // ---- Operations

        public void Add(BackgroundOperation backgroundOperation)
        {
            lock (_backgroundOperationsGuard)
            {
                if (_backgroundOperations.Contains(backgroundOperation))
                    return;

                _backgroundOperations.Add(backgroundOperation);
                ObserveBackgroundOperation(backgroundOperation);

                RecalculateCurrentOperation();
            }
        }

        public void Remove(BackgroundOperation backgroundOperation)
        {
            lock (_backgroundOperationsGuard)
            {
                if (!_backgroundOperations.Contains(backgroundOperation))
                    return;

                _backgroundOperations.Remove(backgroundOperation);
                UnObserveBackgroundOperation(backgroundOperation);

                RecalculateCurrentOperation();
            }
        }

        public void AddRange(IEnumerable<BackgroundOperation> backgroundOperations)
        {
            Argument.ShouldNotBeNull(backgroundOperations, "backgroundOperations");

            lock (_backgroundOperationsGuard)
            {
                foreach (var current in backgroundOperations)
                {
                    if (_backgroundOperations.Contains(current))
                        continue;

                    _backgroundOperations.Add(current);
                    ObserveBackgroundOperation(current);
                }

                RecalculateCurrentOperation();
            }
        }

        private void ObserveBackgroundOperation(BackgroundOperation backgroundOperation)
        {
            backgroundOperation.PropertyChanged += BackgroundOperationChanged;
        }

        private void UnObserveBackgroundOperation(BackgroundOperation backgroundOperation)
        {
            backgroundOperation.PropertyChanged -= BackgroundOperationChanged;
        }

        private void RecalculateCurrentOperation()
        {
            BackgroundOperation currentOperation;

            lock (_backgroundOperationsGuard)
            {
                currentOperation = _backgroundOperations.FirstOrDefault(current => current.IsVisible);
            }

            UiThread.Invoke(() => { CurrentOperation = currentOperation; });
        }

        private void BackgroundOperationChanged(object sender, PropertyChangedEventArgs e)
        {
            RecalculateCurrentOperation();
        }

    }
}
