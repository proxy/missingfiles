﻿using System.ComponentModel;
using MissingFiles.Infrastructure;

namespace MissingFiles.Binding
{
    internal class NotifyPropertyChanged : INotifyPropertyChanged
    {

        // ---- Events

        public event PropertyChangedEventHandler PropertyChanged;

        // ---- Operations

        public void ReportPropertyChanged(string propertyName)
        {
            var eventArgs = new PropertyChangedEventArgs(propertyName);
            OnPropertyChanged(eventArgs);
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs eventArgs)
        {
            UiThread.CheckCrossThreadAccess();

            var handler = PropertyChanged;

            if (handler != null)
                handler(this, eventArgs);
        }

    }
}
