﻿namespace MissingFiles.Models
{
    public sealed class FileEntry : FileSystemEntry
    {

        // ---- Init

        public FileEntry(string relativePath, Directory[] availableInDirectories)
            : base(relativePath, availableInDirectories)
        {
        }

    }
}
