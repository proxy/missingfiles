﻿using System;
using System.Collections.Generic;

namespace MissingFiles.Models.Comparers
{
    public sealed class FileSystemEntryComparer : IEqualityComparer<FileSystemEntry>, IComparer<FileSystemEntry>
    {

        // ---- Fields

        private const int XComesFirst = -1;
        private const int XAndYAreEqual = 0;
        private const int YComesFirst = 1;

        private static readonly Type _directoryType = typeof(DirectoryEntry);

        // ---- Operations

        public bool Equals(FileSystemEntry x, FileSystemEntry y)
        {
            int comparisonResult = Compare(x, y);
            return (comparisonResult == XAndYAreEqual);
        }

        public int GetHashCode(FileSystemEntry obj)
        {
            if ((obj == null) || (obj.RelativePath == null))
                return 0;

            int hashCode = obj.RelativePath.GetHashCode();
            return hashCode;
        }

        public int Compare(FileSystemEntry x, FileSystemEntry y)
        {
            if (x == y)
                return XAndYAreEqual;

            if (x == null)
                return XComesFirst;

            if (y == null)
                return YComesFirst;

            int result = CompareByType(x, y);
            if (result != XAndYAreEqual)
                return result;

            result = CompareByName(x, y);
            return result;
        }

        private static int CompareByType(FileSystemEntry x, FileSystemEntry y)
        {
            var xType = x.GetType();
            var yType = y.GetType();

            if ((xType.IsAssignableFrom(_directoryType)) && (!yType.IsAssignableFrom(_directoryType)))
                return XComesFirst;

            if ((!xType.IsAssignableFrom(_directoryType)) && (yType.IsAssignableFrom(_directoryType)))
                return YComesFirst;

            return XAndYAreEqual;
        }

        private static int CompareByName(FileSystemEntry x, FileSystemEntry y)
        {
            if (x.Name == y.Name)
                return XAndYAreEqual;

            if (x.Name == null)
                return XComesFirst;

            if (y.Name == null)
                return YComesFirst;

            return x.Name.CompareTo(y.Name);
        }

    }
}
