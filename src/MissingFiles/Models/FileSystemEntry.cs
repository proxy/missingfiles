﻿using System;
using System.IO;
using MissingFiles.Infrastructure;

namespace MissingFiles.Models
{
    public abstract class FileSystemEntry
    {

        // ---- Fields

        private readonly string _relativePath;
        private readonly Directory[] _availableInDirectories;

        // ---- Init

        internal FileSystemEntry(string relativePath, Directory[] availableInDirectories)
        {
            Argument.ShouldNotBeNull(relativePath, "relativePath");
            Argument.ShouldNotBeNull(availableInDirectories, "availableInDirectories");

            _relativePath = relativePath;
            _availableInDirectories = availableInDirectories;
        }

        // ---- Properties

        public string RelativePath
        {
            get { return _relativePath; }
        }

        public Directory[] AvailableInDirectories
        {
            get { return _availableInDirectories; }
        }

        public string Name
        {
            get { return Path.GetFileName(_relativePath); }
        }

    }
}
