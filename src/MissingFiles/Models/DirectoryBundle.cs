﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MissingFiles.Collections;
using MissingFiles.Configuration;
using MissingFiles.Extensions;
using MissingFiles.Infrastructure;
using MissingFiles.IO;
using MissingFiles.Models.Comparers;

namespace MissingFiles.Models
{
    public sealed class DirectoryBundle
    {

        // ---- Fields

        private readonly string _name;
        private readonly Directory[] _directories;

        // ---- Init

        public DirectoryBundle(string name, IEnumerable<Directory> directories)
        {
            Argument.ShouldNotBeNull(name, "name");
            Argument.ShouldNotBeNull(directories, "directories");

            _name = name;
            _directories = directories.ToArray();
        }

        // ---- Properties

        public string Name
        {
            get { return _name; }
        }

        public Directory[] Directories
        {
            get { return _directories; }
        }

        // ---- Operations

        public static DirectoryBundle FromConfiguration(DirectoryBundleConfiguration configuration)
        {
            Argument.ShouldNotBeNull(configuration, "configuration");
            Argument.Check(configuration.Name != null, "configuration", "Name cannot be null.");

            IEnumerable<Directory> directories;
            if (configuration.Directories != null)
            {
                directories =
                    (from current in configuration.Directories
                     where (current != null) && (current.Name != null) && (current.Path != null)
                     select Directory.FromConfiguration(current));
            }
            else
            {
                directories = new Directory[0];
            }

            var directoryBundle = new DirectoryBundle(configuration.Name, directories);
            return directoryBundle;
        }

        public MergedDirectoryContents GetMergedDirectoryContents(string relativePath, bool recursive, CancellationToken? cancelToken = null)
        {
            Argument.ShouldNotBeNull(relativePath, "relativePath");

            var errorMessages = new ThreadSafeList<string>();

            var loaderTasks =
                (from current in _directories
                 where (current != null)
                 select Task.Factory.StartNew(() => ListDirectoryContents(current, relativePath, recursive, errorMessages))).ToArray();

            cancelToken.ThrowIfCancellationRequested();
            WaitAllTasks(loaderTasks, cancelToken);

            cancelToken.ThrowIfCancellationRequested();

            var entries =
                (from currentTask in loaderTasks
                 where (currentTask.Result != null)
                 select currentTask.Result).SelectMany(current => current);

            var comparer = new FileSystemEntryComparer();

            var entriesGrouped =
                entries.GroupBy(current => current, comparer);

            var mergedEntries =
                entriesGrouped
                    .Select(current => CreateMergedFileSystemEntry(current.Key, current))
                    .OrderBy(current => current, comparer).ToArray();

            var collectedErrorMessages = errorMessages.ToArray();

            var result = new MergedDirectoryContents(mergedEntries, collectedErrorMessages);
            return result;
        }

        public ObservableFileCopy GetFileCopyCommand(string relativeFilePath, Directory sourceDirectory, Directory targetDirectory)
        {
            Argument.ShouldNotBeNull(relativeFilePath, "relativeFilePath");
            Argument.ShouldNotBeNull(sourceDirectory, "sourceDirectory");
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            var sourcePath = Path.Combine(sourceDirectory.Path, relativeFilePath);
            var targetPath = Path.Combine(targetDirectory.Path, relativeFilePath);

            var fileCopyCommand = new ObservableFileCopy(sourcePath, targetPath);
            return fileCopyCommand;
        }

        public ObservableDirectoryCopy GetDirectoryCopyCommand(FileSystemEntry[] entries, Directory targetDirectory)
        {
            Argument.ShouldNotBeNull(entries, "entries");
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            var directoryList =
                (from current in entries
                 where (current != null) && (current is DirectoryEntry) && (!current.AvailableInDirectories.Contains(targetDirectory))
                 select targetDirectory.GetFullPath(current.RelativePath));

            var fileCopyList =
                (from current in entries
                 where (current != null) && (current is FileEntry) && (!current.AvailableInDirectories.Contains(targetDirectory))
                 let sourceDirectory = current.AvailableInDirectories.FirstOrDefault()
                 where (sourceDirectory != null)
                 select GetFileCopyCommand(current.RelativePath, sourceDirectory, targetDirectory));

            var multiFileCopy = new ObservableDirectoryCopy(directoryList, fileCopyList);
            return multiFileCopy;
        }

        private FileSystemEntry[] ListDirectoryContents(Directory directory, string relativePath, bool recursive, ThreadSafeList<string> errorMessages)
        {
            const string ErrorMessageFormat = "Could not access path '{0}': {1}";

            try
            {
                var entries = directory.GetDirectoryContents(relativePath, recursive);
                return entries;
            }
            catch (OperationCanceledException)
            {
                return null;
            }
            catch (Exception exception)
            {
                string errorMessage = string.Format(ErrorMessageFormat, directory.Path, exception.Message);
                errorMessages.Add(errorMessage);

                return null;
            }
        }

        private static void WaitAllTasks(Task[] loaderTasks, CancellationToken? cancelToken)
        {
            Argument.ShouldNotBeNull(loaderTasks, "loaderTasks");

            if (cancelToken.HasValue)
                Task.WaitAll(loaderTasks, cancelToken.Value);
            else
                Task.WaitAll(loaderTasks);
        }

        private static FileSystemEntry CreateMergedFileSystemEntry(FileSystemEntry key, IEnumerable<FileSystemEntry> allOccurences)
        {
            Argument.ShouldNotBeNull(key, "key");
            Argument.ShouldNotBeNull(allOccurences, "allOccurences");

            var relativePath = key.RelativePath;
            var directories = allOccurences.SelectMany(current => current.AvailableInDirectories).Distinct().ToArray();

            if (key is DirectoryEntry)
                return new DirectoryEntry(relativePath, directories);
            else
                return new FileEntry(relativePath, directories);
        }

    }
}
