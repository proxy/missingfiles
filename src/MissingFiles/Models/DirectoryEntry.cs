﻿namespace MissingFiles.Models
{
    public sealed class DirectoryEntry : FileSystemEntry
    {

        // ---- Init

        public DirectoryEntry(string relativePath, Directory[] availableInDirectories)
            : base(relativePath, availableInDirectories)
        {
        }

    }
}
