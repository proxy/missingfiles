﻿using MissingFiles.Infrastructure;

namespace MissingFiles.Models
{
    public sealed class MergedDirectoryContents
    {

        // ---- Fields

        private readonly FileSystemEntry[] _entries;
        private readonly string[] _errorMessages;

        // ---- Init

        public MergedDirectoryContents(FileSystemEntry[] entries, string[] errorMessages)
        {
            Argument.ShouldNotBeNull(entries, "entries");
            Argument.ShouldNotBeNull(errorMessages, "errorMessages");

            _entries = entries;
            _errorMessages = errorMessages;
        }

        // ---- Properties

        public FileSystemEntry[] Entries
        {
            get { return _entries; }
        }

        public string[] ErrorMessages
        {
            get { return _errorMessages; }
        }

    }
}
