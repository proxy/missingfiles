﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MissingFiles.Configuration;
using MissingFiles.Extensions;
using MissingFiles.Infrastructure;

namespace MissingFiles.Models
{
    public sealed class Directory
    {

        // ---- Fields

        private readonly string _name;
        private readonly string _path;

        // ---- Init

        public Directory(string name, string path)
        {
            Argument.ShouldNotBeNull(name, "name");
            Argument.ShouldNotBeNull(path, "path");

            _name = name;
            _path = path;
        }

        // ---- Properties

        public string Name
        {
            get { return _name; }
        }

        public string Path
        {
            get { return _path; }
        }

        // ---- Operations

        public static Directory FromConfiguration(DirectoryConfiguration configuration)
        {
            Argument.ShouldNotBeNull(configuration, "configuration");
            Argument.Check(configuration.Name != null, "configuration", "Name cannot be null.");
            Argument.Check(configuration.Path != null, "configuration", "Path cannot be null.");

            var directory = new Directory(configuration.Name, configuration.Path);
            return directory;
        }

        public FileSystemEntry[] GetDirectoryContents(string relativePath, bool recursive, CancellationToken? cancelToken = null)
        {
            const string SearchPattern = "*";

            string pathToSearch = _path;

            if (!string.IsNullOrEmpty(relativePath))
                pathToSearch = System.IO.Path.Combine(_path, relativePath);

            cancelToken.ThrowIfCancellationRequested();
            var directoryInfo = new System.IO.DirectoryInfo(pathToSearch);
            if (!directoryInfo.Exists)
                return null;

            var searchOption = (recursive ? System.IO.SearchOption.AllDirectories : System.IO.SearchOption.TopDirectoryOnly);

            cancelToken.ThrowIfCancellationRequested();
            var entries = new List<FileSystemEntry>();

            foreach (var current in directoryInfo.EnumerateFileSystemInfos(SearchPattern, searchOption))
            {
                cancelToken.ThrowIfCancellationRequested();

                if (current.Attributes.HasFlag(System.IO.FileAttributes.Hidden))
                    continue;

                var entry = CreateFileSystemEntry(current);
                entries.Add(entry);
            }

            return entries.ToArray();
        }

        public string GetFullPath(string relativePath)
        {
            Argument.ShouldNotBeNull(relativePath, "relativePath");

            string path = System.IO.Path.Combine(Path, relativePath);
            return path;
        }

        private FileSystemEntry CreateFileSystemEntry(System.IO.FileSystemInfo fileSystemInfo)
        {
            Argument.ShouldNotBeNull(fileSystemInfo, "fileSystemInfo");

            string relativePath = fileSystemInfo.FullName.GetRealativePathTo(_path);
            var availableIn = new[] { this };

            if (fileSystemInfo is System.IO.DirectoryInfo)
                return new DirectoryEntry(relativePath, availableIn);
            else
                return new FileEntry(relativePath, availableIn);
        }

    }
}
