﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MissingFiles.Extensions;
using Color = System.Drawing.Color;

namespace MissingFiles.Controls
{
    partial class WindowButton : UserControl
    {

        // ---- Fields

        private static readonly Dictionary<Color, Color> _mouseOverColorReplacement =
            new Dictionary<Color, Color>
            {
                { Color.Transparent, Color.FromArgb(0xFF, 0xFD, 0xFD, 0xFD) },
                { Color.FromArgb(0xFF, 0x1E, 0x1E, 0x1E), Color.FromArgb(0xFF, 0x00, 0x7A, 0xCC) },
            };

        private static readonly Dictionary<Color, Color> _mouseDownColorReplacement =
            new Dictionary<Color, Color>
            {
                { Color.Transparent, Color.FromArgb(0xFF, 0x00, 0x7A, 0xCC) },
                { Color.FromArgb(0xFF, 0x1E, 0x1E, 0x1E), Color.White },
            };

        // ---- Init

        public WindowButton()
        {
            InitializeComponent();
        }

        // ---- Dependency properties

        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.Register("Image", typeof(BitmapSource), typeof(WindowButton), new PropertyMetadata(OnImageChanged));

        public static readonly DependencyProperty MouseOverImageProperty =
            DependencyProperty.Register("MouseOverImage", typeof(WriteableBitmap), typeof(WindowButton));

        public static readonly DependencyProperty MouseDownImageProperty =
            DependencyProperty.Register("MouseDownImage", typeof(WriteableBitmap), typeof(WindowButton));

        public static readonly DependencyProperty IsMouseActuallyOverProperty =
            DependencyProperty.Register("IsMouseActuallyOver", typeof(bool), typeof(WindowButton), new PropertyMetadata(false));

        public static readonly DependencyProperty IsMouseButtonPressedProperty =
            DependencyProperty.Register("IsMouseButtonPressed", typeof(bool), typeof(WindowButton), new PropertyMetadata(false));

        public static readonly DependencyProperty IsDragStartingPointProperty =
            DependencyProperty.Register("IsDragStartingPoint", typeof(bool), typeof(WindowButton), new PropertyMetadata(false));

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(WindowButton));

        // ---- Properties

        public BitmapSource Image
        {
            get { return (BitmapSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        public WriteableBitmap MouseOverImage
        {
            get { return (WriteableBitmap)GetValue(MouseOverImageProperty); }
            set { SetValue(MouseOverImageProperty, value); }
        }

        public WriteableBitmap MouseDownImage
        {
            get { return (WriteableBitmap)GetValue(MouseDownImageProperty); }
            set { SetValue(MouseDownImageProperty, value); }
        }

        public bool IsMouseActuallyOver
        {
            get { return (bool)GetValue(IsMouseActuallyOverProperty); }
            set { SetValue(IsMouseActuallyOverProperty, value); }
        }

        public bool IsMouseButtonPressed
        {
            get { return (bool)GetValue(IsMouseButtonPressedProperty); }
            set { SetValue(IsMouseButtonPressedProperty, value); }
        }

        public bool IsDragStartingPoint
        {
            get { return (bool)GetValue(IsDragStartingPointProperty); }
            set { SetValue(IsDragStartingPointProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        // ---- Operations

        protected override void OnMouseEnter(MouseEventArgs eventArgs)
        {
            base.OnMouseEnter(eventArgs);

            bool isLeftButtonPressed = (Mouse.LeftButton == MouseButtonState.Pressed);

            IsMouseActuallyOver = true;
            IsMouseButtonPressed = isLeftButtonPressed;
        }

        protected override void OnMouseMove(MouseEventArgs eventArgs)
        {
            base.OnMouseMove(eventArgs);

            var touchPoint = eventArgs.GetPosition(this);
            IsMouseActuallyOver = CalculateIsMouseActuallyOver(touchPoint);
        }

        protected override void OnMouseLeave(MouseEventArgs eventArgs)
        {
            base.OnMouseLeave(eventArgs);

            bool isLeftButtonPressed = (Mouse.LeftButton == MouseButtonState.Pressed);

            IsMouseActuallyOver = false;
            IsMouseButtonPressed = isLeftButtonPressed;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs eventArgs)
        {
            base.OnMouseLeftButtonDown(eventArgs);

            IsMouseActuallyOver = true;
            IsMouseButtonPressed = true;
            IsDragStartingPoint = true;

            CaptureMouse();

            eventArgs.Handled = true;
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs eventArgs)
        {
            base.OnMouseLeftButtonUp(eventArgs);

            var touchPoint = eventArgs.GetPosition(this);
            IsMouseActuallyOver = CalculateIsMouseActuallyOver(touchPoint);
            IsMouseButtonPressed = false;
            IsDragStartingPoint = false;
            
            ReleaseMouseCapture();

            if (IsMouseActuallyOver)
            {
                ExecuteCommand();
                eventArgs.Handled = true;
            }
        }

        private static void OnImageChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            var instance = (dependencyObject as WindowButton);
            if (instance == null)
                return;

            instance.UpdateSpecialImages();
        }

        private void UpdateSpecialImages()
        {
            MouseOverImage = Image.ToWriteable().ReplaceColors(_mouseOverColorReplacement);
            MouseDownImage = Image.ToWriteable().ReplaceColors(_mouseDownColorReplacement);
        }

        private bool CalculateIsMouseActuallyOver(Point touchPoint)
        {
            return
                ((touchPoint.X >= 0.0) && (touchPoint.Y >= 0.0) &&
                 (touchPoint.X <= ActualWidth) && (touchPoint.Y <= ActualHeight));
        }

        private void ExecuteCommand()
        {
            var command = Command;

            if (command != null)
                command.Execute(null);
        }

    }
}
