﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using MissingFiles.Binding;
using MissingFiles.Infrastructure;

namespace MissingFiles.Controls
{
    internal class BorderlessWindow : Window
    {

        // ---- Fields

        private Grid _layoutGrid;

        private bool _isResizing = false;
        private Point _resizingReferencePoint;
        private double _originalWidth;
        private double _originalHeight;
        private double _originalLeft;
        private double _originalTop;
        private WindowResizerLocation _resizerLocation;

        private readonly Command _minimizeCommand;
        private readonly Command _maximizeCommand;
        private readonly Command _closeCommand;

        // ---- Init

        static BorderlessWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BorderlessWindow), new FrameworkPropertyMetadata(typeof(BorderlessWindow)));
        }

        public BorderlessWindow()
        {
            _minimizeCommand = new Command(_ => Minimize(), null);
            _maximizeCommand = new Command(_ => Maximize(), null);
            _closeCommand = new Command(_ => Close(), null);
        }

        // ---- Properties

        public Command MinimizeCommand
        {
            get { return _minimizeCommand; }
        }

        public Command MaximizeCommand
        {
            get { return _maximizeCommand; }
        }

        public Command CloseCommand
        {
            get { return _closeCommand; }
        }

        // ---- Operations

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _layoutGrid = (GetTemplateChild("_layoutGrid") as Grid);

            SetupResizers();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs eventArgs)
        {
            base.OnMouseLeftButtonDown(eventArgs);

            if (eventArgs.Handled)
                return;

            if (eventArgs.ClickCount == 2)
            {
                Maximize();
            }
            else
            {
                if (WindowState == System.Windows.WindowState.Maximized)
                {
                    var maximizedWindowPosition = eventArgs.GetPosition(this);
                    RestoreToNormalKeepingMousePosition(maximizedWindowPosition);
                }

                DragMove();
            }

            eventArgs.Handled = true;
        }

        private void SetupResizers()
        {
            if (_layoutGrid == null)
                return;

            var resizers = _layoutGrid.Children.OfType<Rectangle>();
            foreach (var current in resizers)
                SetupResizer(current);
        }

        private void SetupResizer(Rectangle resizer)
        {
            Argument.ShouldNotBeNull(resizer, "resizer");

            resizer.MouseLeftButtonDown += Resizer_MouseLeftButtonDown;
            resizer.MouseLeftButtonUp += Resizer_MouseLeftButtonUp;
            resizer.MouseMove += Resizer_MouseMove;
        }

        private void Resizer_MouseLeftButtonDown(object sender, MouseButtonEventArgs eventArgs)
        {
            var frameworkElement = (sender as FrameworkElement);
            if (frameworkElement == null)
                return;

            _resizerLocation = GetResizerLocationForControl(frameworkElement);
            if (_resizerLocation == WindowResizerLocation.None)
                return;

            _isResizing = true;
            _resizingReferencePoint = GetScreenPosition(eventArgs);
            _originalWidth = Width;
            _originalHeight = Height;
            _originalLeft = Left;
            _originalTop = Top;

            eventArgs.Handled = true;

            frameworkElement.CaptureMouse();
        }

        private void Resizer_MouseLeftButtonUp(object sender, MouseButtonEventArgs eventArgs)
        {
            if (!_isResizing)
                return;

            var screenPosition = GetScreenPosition(eventArgs);
            Resize(screenPosition);

            _isResizing = false;

            var uiElement = (sender as UIElement);
            if (uiElement == null)
                return;

            uiElement.ReleaseMouseCapture();
        }

        private void Resizer_MouseMove(object sender, MouseEventArgs eventArgs)
        {
            if (!_isResizing)
                return;

            var screenPosition = GetScreenPosition(eventArgs);
            Resize(screenPosition);

            eventArgs.Handled = true;
        }

        private void Resize(Point screenPosition)
        {
            ResizeHorizontally(screenPosition);
            ResizeVertically(screenPosition);
        }

        private void ResizeHorizontally(Point screenPosition)
        {
            if (!_resizerLocation.HasFlag(WindowResizerLocation.Horizontal))
                return;

            double widthChange = (_resizingReferencePoint.X - screenPosition.X);
            if (_resizerLocation.HasFlag(WindowResizerLocation.HorizontalRegionEnd))
                widthChange = -widthChange;

            if ((_originalWidth + widthChange) < MinWidth)
                widthChange = (MinWidth - _originalWidth);

            Width = (_originalWidth + widthChange);

            if (_resizerLocation.HasFlag(WindowResizerLocation.HorizontalRegionStart))
                Left = (_originalLeft - widthChange);
        }

        private void ResizeVertically(Point screenPosition)
        {
            if (!_resizerLocation.HasFlag(WindowResizerLocation.Vertical))
                return;

            double heightChange = (_resizingReferencePoint.Y - screenPosition.Y);
            if (_resizerLocation.HasFlag(WindowResizerLocation.VerticalRegionEnd))
                heightChange = -heightChange;

            if ((_originalHeight + heightChange) < MinHeight)
                heightChange = (MinHeight - _originalHeight);

            Height = (_originalHeight + heightChange);

            if (_resizerLocation.HasFlag(WindowResizerLocation.VerticalRegionStart))
                Top = (_originalTop - heightChange);
        }

        private Point GetScreenPosition(MouseEventArgs eventArgs)
        {
            var mousePosition = eventArgs.GetPosition(this);
            var screenPosition = PointToScreen(mousePosition);

            return screenPosition;
        }

        private WindowResizerLocation GetResizerLocationForControl(FrameworkElement frameworkElement)
        {
            Argument.ShouldNotBeNull(frameworkElement, "frameworkElement");

            var tag = frameworkElement.Tag;
            if (!(tag is WindowResizerLocation))
                return WindowResizerLocation.None;

            var resizerLocation = (WindowResizerLocation)tag;
            return resizerLocation;
        }

        private void Minimize()
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        private void Maximize()
        {
            WindowState = ((WindowState == WindowState.Maximized) ? WindowState.Normal : WindowState.Maximized);
        }

        private void RestoreToNormalKeepingMousePosition(Point maximizedWindowPosition)
        {
            var absolutePosition = PointToScreen(maximizedWindowPosition);
            double maximizedWidth = ActualWidth;
            double maximizedHeight = ActualHeight;

            if ((maximizedWidth == 0.0) || (maximizedHeight == 0.0))
                return;

            WindowState = System.Windows.WindowState.Normal;

            double normalWidth = ActualWidth;
            double normalHeight = ActualHeight;

            double maximizedToNormalXRatio = (normalWidth / maximizedWidth);
            double maximizedToNormalYRatio = (normalHeight / maximizedHeight);

            double maximizedXOffset = maximizedWindowPosition.X;
            double maximizedYOffset = maximizedWindowPosition.Y;

            double normalXOffset = System.Math.Round(maximizedXOffset * maximizedToNormalXRatio);
            double normalYOffset = System.Math.Round(maximizedYOffset * maximizedToNormalYRatio);

            Left = (absolutePosition.X - normalXOffset);
            Top = (absolutePosition.Y - normalYOffset);
        }

    }
}
