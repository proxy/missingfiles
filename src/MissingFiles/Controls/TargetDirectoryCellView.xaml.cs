﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MissingFiles.Binding;
using MissingFiles.ViewModels.Explore;

namespace MissingFiles.Controls
{
    partial class TargetDirectoryCellView : UserControl
    {

        // ---- Init

        public TargetDirectoryCellView()
        {
            InitializeComponent();
        }

        // ---- Dependency properties

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(FileSystemEntryViewModel), typeof(TargetDirectoryCellView),
                new PropertyMetadata(PropertyChanged));

        public static DependencyProperty DirectoryIndexProperty =
            DependencyProperty.Register("DirectoryIndex", typeof(int), typeof(TargetDirectoryCellView),
                new PropertyMetadata(PropertyChanged));

        // ---- Properties

        public FileSystemEntryViewModel ViewModel
        {
            get { return (FileSystemEntryViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public int DirectoryIndex
        {
            get { return (int)GetValue(DirectoryIndexProperty); }
            set { SetValue(DirectoryIndexProperty, value); }
        }

        // ---- Operations

        private static void PropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            var instance = (dependencyObject as TargetDirectoryCellView);
            if (instance == null)
                return;

            instance.OnChanged(eventArgs);
        }

        private void OnChanged(DependencyPropertyChangedEventArgs eventArgs)
        {
            BindCheckBox();

            if (eventArgs.Property == DirectoryIndexProperty)
                Resources["DirectoryIndex"] = eventArgs.NewValue;
        }

        private void BindCheckBox()
        {
            const string PropertyPathFormat = "Availability[{0}]";

            string propertyPath = string.Format(PropertyPathFormat, DirectoryIndex);
            var binding = new System.Windows.Data.Binding(propertyPath);

            binding.Mode = BindingMode.TwoWay;

            _checkBox.SetBinding(CheckBox.IsCheckedProperty, binding);
        }

    }
}
