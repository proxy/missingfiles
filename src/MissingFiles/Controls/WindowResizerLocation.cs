﻿using System;

namespace MissingFiles.Controls
{
    [Flags]
    internal enum WindowResizerLocation : int
    {
        None = 0x00,
        Horizontal = 0x01,
        Vertical = 0x02,
        HorizontalRegionStart = 0x04,
        HorizontalRegionEnd = 0x08,
        VerticalRegionStart = 0x10,
        VerticalRegionEnd = 0x20,
        Left = (Horizontal | HorizontalRegionStart),
        Top = (Vertical | VerticalRegionStart),
        Right = (Horizontal | HorizontalRegionEnd),
        Bottom = (Vertical | VerticalRegionEnd),
        TopLeft = (Top | Left),
        TopRight = (Top | Right),
        BottomLeft = (Bottom | Left),
        BottomRight = (Bottom | Right),
    }
}
