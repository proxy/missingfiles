﻿using System.Windows;
using System.Windows.Controls;
using MissingFiles.Infrastructure;
using MissingFiles.ViewModels.Explore;

namespace MissingFiles.Controls
{
    partial class TargetDirectoryColumn : GridViewColumn
    {

        // ---- Fields

        private readonly int _targetDirectoryIndex;
        private readonly DirectoryTargetViewModel _targetDirectory;

        // ---- Init

        public TargetDirectoryColumn(int targetDirectoryIndex, DirectoryTargetViewModel targetDirectory)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            InitializeComponent();

            _targetDirectoryIndex = targetDirectoryIndex;
            _targetDirectory = targetDirectory;
            _headerTextBlock.Text = _targetDirectory.Name;
            CellTemplate = CreateCellTemplate();
        }

        // ---- Properties

        public DirectoryTargetViewModel TargetDirectory
        {
            get { return _targetDirectory; }
        }

        // ---- Operations

        private DataTemplate CreateCellTemplate()
        {
            // <DataTemplate>
            //    <v:TargetDirectoryColumnCellView DirectoryIndex="{0}" ViewModel="{Binding}" />
            // </DataTemplate>

            var factory = new FrameworkElementFactory(typeof(TargetDirectoryCellView));

            factory.SetValue(TargetDirectoryCellView.DirectoryIndexProperty, _targetDirectoryIndex);
            factory.SetBinding(TargetDirectoryCellView.ViewModelProperty, new System.Windows.Data.Binding());

            var template = new DataTemplate();
            template.VisualTree = factory;

            return template;
        }

    }
}
