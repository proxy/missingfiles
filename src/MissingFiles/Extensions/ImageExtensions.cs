﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MissingFiles.Infrastructure;
using Color = System.Drawing.Color;

namespace MissingFiles.Extensions
{
    internal static class ImageExtensions
    {

        // ---- Operations

        public static WriteableBitmap ToWriteable(this BitmapSource instance)
        {
            if (instance == null)
                return null;

            var formatConvertedBitmap = new FormatConvertedBitmap(instance, PixelFormats.Pbgra32, null, 0.0);
            var writeableBitmap = new WriteableBitmap(formatConvertedBitmap);
            return writeableBitmap;
        }

        public static WriteableBitmap ReplaceColors(this WriteableBitmap instance, Dictionary<Color, Color> replacementPairs)
        {
            if (instance == null)
                return null;

            Argument.Check(instance.Format == PixelFormats.Pbgra32, "instance", "The format of the image must be Pbgra32 (ARGB)!");

            instance.Lock();
            try
            {
                ReplaceColorsInner(instance, replacementPairs);

                var bitmapRect = new Int32Rect(0, 0, instance.PixelWidth, instance.PixelHeight);
                instance.AddDirtyRect(bitmapRect);
            }
            finally
            {
                instance.Unlock();
            }

            return instance;
        }

        private static void ReplaceColorsInner(WriteableBitmap writeableBitmap, Dictionary<Color, Color> replacementPairs)
        {
            const int BytesPerPixel = 4;

            Argument.ShouldNotBeNull(writeableBitmap, "writeableBitmap");

            int width = writeableBitmap.PixelWidth;
            int height = writeableBitmap.PixelHeight;
            int stride = writeableBitmap.BackBufferStride;

            unsafe
            {
                IntPtr backBuffer = writeableBitmap.BackBuffer;

                for (int currentY = 0; currentY < height; currentY++)
                {
                    IntPtr currentYAddress = (backBuffer + (currentY * stride));

                    for (int currentX = 0; currentX < width; currentX++)
                    {
                        Int32* currentAddress = (int*)(currentYAddress + (currentX * BytesPerPixel));

                        var currentColor = Color.FromArgb(*currentAddress);
                        var replacementColor = ReplaceColor(currentColor, replacementPairs);
                        (*currentAddress) = replacementColor.ToArgb();
                    }
                }
            }
        }

        private static Color ReplaceColor(Color currentColor, Dictionary<Color, Color> replacementPairs)
        {
            foreach (var currentPair in replacementPairs)
            {
                bool areClose = AreColorsClose(currentColor, currentPair.Key);
                if (areClose)
                    return currentPair.Value;
            }

            return currentColor;
        }

        private static bool AreColorsClose(Color color1, Color color2)
        {
            const int Tolerance = 10;

            if ((color1.A < Tolerance) && (color2.A < Tolerance))
                return true;

            bool areClose =
                (Math.Abs(color1.A - color2.A) < Tolerance) &&
                (Math.Abs(color1.R - color2.R) < Tolerance) &&
                (Math.Abs(color1.G - color2.G) < Tolerance) &&
                (Math.Abs(color1.B - color2.B) < Tolerance);

            return areClose;
        }

    }
}
