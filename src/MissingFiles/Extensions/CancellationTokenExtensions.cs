﻿using System.Threading;

namespace MissingFiles.Extensions
{
    internal static class CancellationTokenExtensions
    {

        // ---- Operations

        public static void ThrowIfCancellationRequested(this CancellationToken? instance)
        {
            if (!instance.HasValue)
                return;

            instance.Value.ThrowIfCancellationRequested();
        }

    }
}
