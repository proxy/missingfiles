﻿using System;
using System.IO;

namespace MissingFiles.Extensions
{
    internal static class PathExtensions
    {

        // ---- Operations

        public static string AsPath(this string instance)
        {
            if (instance == null)
                return instance;

            string result = instance.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            return result;
        }

        public static string GetRealativePathTo(this string instance, string basePath)
        {
            var instanceTrimmed = instance.AsPath();
            var basePathTrimmed = basePath.AsPath();

            if (string.IsNullOrEmpty(instanceTrimmed))
                return instanceTrimmed;

            if (string.IsNullOrEmpty(basePathTrimmed))
                return instanceTrimmed;

            if (!instanceTrimmed.StartsWith(basePathTrimmed, StringComparison.InvariantCultureIgnoreCase))
                return instance;

            var result = instanceTrimmed.Substring(basePathTrimmed.Length).TrimStart(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            return result;
        }

    }
}
