﻿using MissingFiles.Infrastructure;
using MissingFiles.IO;

namespace MissingFiles.Extensions
{
    internal static class CopyProgressEventArgsExtensions
    {

        // ---- Operations

        public static string ToStatusText(this FileCopyProgressEventArgs instance)
        {
            const string StatusTextFormat = "Copying {0}: {1:0.##} MB of {2:0.##} MB";

            Argument.ShouldNotBeNull(instance, "instance");

            string statusText = string.Format(StatusTextFormat, instance.DestinationFilePath, instance.TransferredMegaBytes, instance.TotalMegaBytes);
            return statusText;
        }

        public static string ToStatusText(this MultiFileCopyProgressEventArgs instance)
        {
            const string DirectoryCopyStatusTextFormat = "Copying file {0} of {1}";

            Argument.ShouldNotBeNull(instance, "instance");

            string statusText = string.Format(DirectoryCopyStatusTextFormat, (instance.FinishedFilesCount + 1), instance.TotalFilesCount);
            return statusText;
        }

    }
}
