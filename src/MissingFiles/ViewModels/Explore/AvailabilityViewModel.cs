﻿using System;
using System.Collections.Generic;
using System.Linq;
using MissingFiles.Binding;
using MissingFiles.Infrastructure;

namespace MissingFiles.ViewModels.Explore
{
    internal class AvailabilityViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly FileSystemEntryViewModel _entry;
        private readonly DirectoryTargetViewModel[] _allDirectories;
        private readonly Dictionary<DirectoryTargetViewModel, bool> _availableInDirectories;

        // ---- Init

        public AvailabilityViewModel(FileSystemEntryViewModel entry, IEnumerable<DirectoryTargetViewModel> allDirectories, IEnumerable<DirectoryTargetViewModel> targetDirectories)
        {
            Argument.ShouldNotBeNull(entry, "entry");
            Argument.ShouldNotBeNull(allDirectories, "allDirectories");

            _entry = entry;
            _allDirectories = allDirectories.ToArray();

            _availableInDirectories =
                _allDirectories.ToDictionary(current => current, current => (targetDirectories != null) && (targetDirectories.Contains(current)));
        }

        // ---- Properties

        public DirectoryTargetViewModel[] AllDirectories
        {
            get { return _allDirectories; }
        }

        public bool this[int indexer]
        {
            get
            {
                if ((indexer < 0) || (indexer >= _allDirectories.Length))
                    throw new ArgumentOutOfRangeException("indexer");

                var directory = _allDirectories[indexer];

                bool value;
                if (!_availableInDirectories.TryGetValue(directory, out value))
                    return false;

                return value;
            }
            set
            {
                if (this[indexer] == value)
                    return;

                if ((indexer < 0) || (indexer >= _allDirectories.Length))
                    throw new ArgumentOutOfRangeException("indexer");

                var directory = _allDirectories[indexer];
                _entry.AvailabilityChangeRequested(directory, value);
            }
        }

        // ---- Operations

        public void UpdateAvailability(DirectoryTargetViewModel directory, bool value)
        {
            _availableInDirectories[directory] = value;

            ReportPropertyChanged(System.Windows.Data.Binding.IndexerName);
        }

        public DirectoryTargetViewModel GetSourceDirectory()
        {
            var sources = GetSourceDirectories();
            var source = sources.FirstOrDefault();
            return source;
        }

        public DirectoryTargetViewModel[] GetSourceDirectories()
        {
            var sources =
                (from current in _availableInDirectories
                 where current.Value
                 select current.Key).ToArray();

            return sources;
        }

    }
}
