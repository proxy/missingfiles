﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using MissingFiles.Binding;
using MissingFiles.Extensions;
using MissingFiles.Infrastructure;
using MissingFiles.IO;

namespace MissingFiles.ViewModels.Explore
{
    internal class DirectoryEntryViewModel : FileSystemEntryViewModel
    {

        // ---- Fields

        private readonly bool _isParentReference;

        // ---- Init

        public DirectoryEntryViewModel(DirectoryBundleExploreViewModel mainViewModel, string relativePath, bool isParentReference, IEnumerable<DirectoryTargetViewModel> availableInDirectories)
            : base(mainViewModel, relativePath, availableInDirectories)
        {
            _isParentReference = isParentReference;
        }

        // ---- Properties

        public override string Type
        {
            get { return "Directory"; }
        }

        public bool IsParentReference
        {
            get { return _isParentReference; }
        }

        // ---- Operations

        protected override string GetDisplayName()
        {
            const string DisplayNameFormat = "[{0}]";

            string targetDirectoryName;
            if (!_isParentReference)
                targetDirectoryName = Path.GetFileName(RelativePath);
            else
                targetDirectoryName = "..";

            string displayName = string.Format(DisplayNameFormat, targetDirectoryName);
            return displayName;
        }

        protected override bool CanCopy(object parameter)
        {
            if (_isParentReference)
                return false;

            int directoryIndex;
            bool isDirectoryIndexValid = TryParseDirectoryIndex(parameter, out directoryIndex);
            return isDirectoryIndexValid;
        }

        protected override bool CanDelete(object parameter)
        {
            if (_isParentReference)
                return false;

            return base.CanDelete(parameter);
        }

        protected override void DeployTo(DirectoryTargetViewModel targetDirectory)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            if (_isParentReference)
                return;

            var backgroundOperation =
                new BackgroundOperation((operation, cancelToken) => DeployToBackground(operation, targetDirectory, cancelToken), true)
                {
                    CanReportProgress = true,
                };

            DirectoryExploreViewModel.ExecuteImmediateOperation(backgroundOperation);
        }

        protected override void DeleteFrom(DirectoryTargetViewModel targetDirectory)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            if (_isParentReference)
                return;

            bool deleteConfirmed = ConfirmDelete(targetDirectory);
            if (!deleteConfirmed)
                return;

            string directoryPath = targetDirectory.Directory.GetFullPath(RelativePath);

            var backgroundOperation =
                new BackgroundOperation(
                    (operation, cancelToken) =>
                    {
                        Directory.Delete(directoryPath, true);
                        UiThread.Invoke(() => UpdateAvailability(targetDirectory, false));
                    }, false);

            DirectoryExploreViewModel.ExecuteImmediateOperation(backgroundOperation);
        }

        private void DeployToBackground(BackgroundOperation operation, DirectoryTargetViewModel targetDirectory, CancellationToken cancelToken)
        {
            const int CancelCheckTimeoutMilliseconds = 500;
            const string RetrieveingFileListMessage = "Retrieving file list...";

            Argument.ShouldNotBeNull(operation, "operation");
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            UiThread.Invoke(() => { operation.StatusText = RetrieveingFileListMessage; });

            var directoryBundle = DirectoryExploreViewModel.DirectoryBundle;
            var mergedContents = directoryBundle.GetMergedDirectoryContents(RelativePath, true, cancelToken);

            if ((mergedContents == null) || (mergedContents.Entries == null))
                return;

            if ((mergedContents.ErrorMessages != null) && (mergedContents.ErrorMessages.Length > 0))
                operation.AddErrors(mergedContents.ErrorMessages);

            var directoryCopyCommand = directoryBundle.GetDirectoryCopyCommand(mergedContents.Entries, targetDirectory.Directory);
            directoryCopyCommand.ProgressChanged += (sender, eventArgs) => DirectoryCopy_ProgressChanged(operation, eventArgs);
            directoryCopyCommand.ErrorOccured += (sender, eventArgs) => DirectoryCopy_ErrorOccured(operation, eventArgs);

            var task = Task.Factory.StartNew(() => directoryCopyCommand.Execute());

            bool cancelled = false;
            while ((!cancelled) && (!task.IsCompleted))
            {
                cancelled = cancelToken.WaitHandle.WaitOne(CancelCheckTimeoutMilliseconds);
            }

            if (cancelled)
                directoryCopyCommand.Cancel();

            task.Wait();

            if (!cancelled)
                UiThread.Invoke(() => UpdateAvailability(targetDirectory, true));
        }

        private void DirectoryCopy_ProgressChanged(BackgroundOperation operation, MultiFileCopyProgressEventArgs eventArgs)
        {
            Argument.ShouldNotBeNull(operation, "operation");
            Argument.ShouldNotBeNull(eventArgs, "eventArgs");

            UiThread.BeginInvoke(
                () =>
                {
                    operation.HasSubOperation = true;
                    operation.StatusText = eventArgs.ToStatusText();
                    operation.CurrentProgress = eventArgs.Progress;
                    operation.SubOperationStatusText = eventArgs.CurrentFileProgress.ToStatusText();
                    operation.SubOperationProgress = eventArgs.CurrentFileProgress.Progress;
                });
        }

        private void DirectoryCopy_ErrorOccured(BackgroundOperation operation, ErrorOccuredEventArgs eventArgs)
        {
            Argument.ShouldNotBeNull(operation, "operation");
            Argument.ShouldNotBeNull(eventArgs, "eventArgs");

            UiThread.BeginInvoke(
                () =>
                {
                    operation.AddError(eventArgs.ErrorMessage);
                });
        }

        private bool ConfirmDelete(DirectoryTargetViewModel targetDirectory)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            var sourceDirectories = Availability.GetSourceDirectories();
            bool isLastSource = (sourceDirectories.Length <= 1);

            string confirmationMessage = GetConfirmationMessage(targetDirectory, isLastSource);
            var questionImage = (isLastSource ? MessageBoxImage.Warning : MessageBoxImage.Question);
            var confirmationResult = MessageBox.Show(confirmationMessage, "Confirm delete", MessageBoxButton.YesNo, questionImage);

            return (confirmationResult == MessageBoxResult.Yes);
        }

        private string GetConfirmationMessage(DirectoryTargetViewModel targetDirectory, bool isLastSource)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            List<string> confirmationMessageLines = new List<string>();

            if (isLastSource)
                confirmationMessageLines.Add("This directory cannot be found in any other location.");

            string fullPath = targetDirectory.Directory.GetFullPath(RelativePath);
            string questionLine = string.Format("Are you sure you want to delete '{0}'?", fullPath);
            confirmationMessageLines.Add(questionLine);

            string message = string.Join("\n", confirmationMessageLines);
            return message;
        }

    }
}
