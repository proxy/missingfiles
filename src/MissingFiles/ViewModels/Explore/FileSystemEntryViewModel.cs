﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using MissingFiles.Binding;
using MissingFiles.Infrastructure;

namespace MissingFiles.ViewModels.Explore
{
    internal abstract class FileSystemEntryViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly DirectoryBundleExploreViewModel _directoryExploreViewModel;
        private readonly string _relativePath;
        private readonly string _name;
        private readonly AvailabilityViewModel _availability;

        private readonly Lazy<string> _displayNameField;

        private readonly Command _copyCommand;
        private readonly Command _deleteCommand;

        // ---- Init

        public FileSystemEntryViewModel(DirectoryBundleExploreViewModel directoryExploreViewModel, string relativePath, IEnumerable<DirectoryTargetViewModel> availableInDirectories)
        {
            Argument.ShouldNotBeNull(directoryExploreViewModel, "directoryExploreViewModel");
            Argument.ShouldNotBeNull(relativePath, "relativePath");

            _directoryExploreViewModel = directoryExploreViewModel;
            _relativePath = relativePath;
            _name = Path.GetFileName(_relativePath);
            _availability = new AvailabilityViewModel(this, directoryExploreViewModel.TargetDirectories, availableInDirectories);

            _displayNameField = new Lazy<string>(GetDisplayName, LazyThreadSafetyMode.ExecutionAndPublication);

            _copyCommand = new Command(parameter => Copy(parameter), parameter => CanCopy(parameter));
            _deleteCommand = new Command(parameter => Delete(parameter), parameter => CanDelete(parameter));
        }

        // ---- Properties

        public DirectoryBundleExploreViewModel DirectoryExploreViewModel
        {
            get { return _directoryExploreViewModel; }
        }

        public string RelativePath
        {
            get { return _relativePath; }
        }

        public abstract string Type
        {
            get;
        }

        public string Name
        {
            get { return _name; }
        }

        public AvailabilityViewModel Availability
        {
            get { return _availability; }
        }

        public string DisplayName
        {
            get { return _displayNameField.Value; }
        }

        public Command CopyCommand
        {
            get { return _copyCommand; }
        }

        public Command DeleteCommand
        {
            get { return _deleteCommand; }
        }

        // ---- Operations

        public void AvailabilityChangeRequested(DirectoryTargetViewModel targetDirectory, bool newAvailability)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            if (newAvailability)
                DeployTo(targetDirectory);
            else
                DeleteFrom(targetDirectory);
        }

        protected abstract string GetDisplayName();

        protected abstract void DeployTo(DirectoryTargetViewModel targetDirectory);

        protected abstract void DeleteFrom(DirectoryTargetViewModel targetDirectory);

        protected virtual bool CanCopy(object parameter)
        {
            int directoryIndex;
            bool isDirectoryIndexValid = TryParseDirectoryIndex(parameter, out directoryIndex);
            if (!isDirectoryIndexValid)
                return false;

            bool canCopy = (!Availability[directoryIndex]);
            return canCopy;
        }

        protected virtual void Copy(object parameter)
        {
            int directoryIndex;
            bool isDirectoryIndexValid = TryParseDirectoryIndex(parameter, out directoryIndex);
            if (!isDirectoryIndexValid)
                return;

            var targetDirectory = _availability.AllDirectories[directoryIndex];
            if (targetDirectory == null)
                return;

            DeployTo(targetDirectory);
        }

        protected virtual bool CanDelete(object parameter)
        {
            int directoryIndex;
            bool isDirectoryIndexValid = TryParseDirectoryIndex(parameter, out directoryIndex);
            if (!isDirectoryIndexValid)
                return false;

            bool canDelete = Availability[directoryIndex];
            return canDelete;
        }

        protected virtual void Delete(object parameter)
        {
            int directoryIndex;
            bool isDirectoryIndexValid = TryParseDirectoryIndex(parameter, out directoryIndex);
            if (!isDirectoryIndexValid)
                return;

            var targetDirectory = _availability.AllDirectories[directoryIndex];
            if (targetDirectory == null)
                return;

            DeleteFrom(targetDirectory);
        }

        protected void UpdateAvailability(DirectoryTargetViewModel targetDirectory, bool value)
        {
            _availability.UpdateAvailability(targetDirectory, value);

            _copyCommand.ReportCanExecuteChanged();
            _deleteCommand.ReportCanExecuteChanged();
        }

        protected bool TryParseDirectoryIndex(object directoryIndexParameter, out int directoryIndex)
        {
            if (!(directoryIndexParameter is int))
            {
                directoryIndex = 0;
                return false;
            }

            directoryIndex = (int)directoryIndexParameter;
            return ((directoryIndex >= 0) && (directoryIndex < _availability.AllDirectories.Length));
        }

    }
}
