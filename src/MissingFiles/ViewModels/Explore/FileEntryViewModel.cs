﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using MissingFiles.Binding;
using MissingFiles.Extensions;
using MissingFiles.Infrastructure;
using MissingFiles.IO;
using MissingFiles.Models;

namespace MissingFiles.ViewModels.Explore
{
    internal class FileEntryViewModel : FileSystemEntryViewModel
    {

        // ---- Init

        public FileEntryViewModel(DirectoryBundleExploreViewModel mainViewModel, string relativePath, IEnumerable<DirectoryTargetViewModel> availableInDirectories)
            : base(mainViewModel, relativePath, availableInDirectories)
        {
        }

        // ---- Properties

        public override string Type
        {
            get { return "File"; }
        }

        // ---- Operations

        protected override string GetDisplayName()
        {
            return Path.GetFileName(RelativePath);
        }

        protected override void DeployTo(DirectoryTargetViewModel targetDirectory)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            var backgroundOperation =
                new BackgroundOperation((operation, cancelToken) => DeployToBackground(operation, targetDirectory, cancelToken), true)
                {
                    CanReportProgress = true,
                };

            DirectoryExploreViewModel.ExecuteImmediateOperation(backgroundOperation);
        }

        protected override void DeleteFrom(DirectoryTargetViewModel targetDirectory)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            bool deleteConfirmed = ConfirmDelete(targetDirectory);
            if (!deleteConfirmed)
                return;

            string filePath = targetDirectory.Directory.GetFullPath(RelativePath);

            var backgroundOperation =
                new BackgroundOperation(
                    (operation, cancelToken) =>
                    {
                        File.Delete(filePath);
                        UiThread.Invoke(() => UpdateAvailability(targetDirectory, false));
                    }, false);

            DirectoryExploreViewModel.ExecuteImmediateOperation(backgroundOperation);
        }

        private void DeployToBackground(BackgroundOperation operation, DirectoryTargetViewModel targetDirectory, CancellationToken cancelToken)
        {
            const int CancelCheckTimeoutMilliseconds = 200;

            Argument.ShouldNotBeNull(operation, "operation");
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            var sourceDirectory = Availability.GetSourceDirectory();
            var fileCopyCommand = DirectoryExploreViewModel.DirectoryBundle.GetFileCopyCommand(RelativePath, sourceDirectory.Directory, targetDirectory.Directory);

            fileCopyCommand.ProgressChanged +=
                (sender, eventArgs) =>
                {
                    Argument.ShouldNotBeNull(eventArgs, "eventArgs");

                    UiThread.BeginInvoke(
                        () =>
                        {
                            operation.CurrentProgress = eventArgs.Progress;
                            operation.StatusText = eventArgs.ToStatusText();
                        });
                };

            var task = Task.Factory.StartNew(() => fileCopyCommand.Execute());

            bool cancelled = false;
            while ((!cancelled) && (!task.IsCompleted))
            {
                cancelled = cancelToken.WaitHandle.WaitOne(CancelCheckTimeoutMilliseconds);
            }

            if (cancelled)
                fileCopyCommand.Cancel();

            task.Wait();

            if (!cancelled)
                UiThread.Invoke(() => UpdateAvailability(targetDirectory, true));
        }

        private bool ConfirmDelete(DirectoryTargetViewModel targetDirectory)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            var sourceDirectories = Availability.GetSourceDirectories();
            bool isLastSource = (sourceDirectories.Length <= 1);

            string confirmationMessage = GetConfirmationMessage(targetDirectory, isLastSource);
            var questionImage = (isLastSource ? MessageBoxImage.Warning : MessageBoxImage.Question);
            var confirmationResult = MessageBox.Show(confirmationMessage, "Confirm delete", MessageBoxButton.YesNo, questionImage);

            return (confirmationResult == MessageBoxResult.Yes);
        }

        private string GetConfirmationMessage(DirectoryTargetViewModel targetDirectory, bool isLastSource)
        {
            Argument.ShouldNotBeNull(targetDirectory, "targetDirectory");

            List<string> confirmationMessageLines = new List<string>();

            if (isLastSource)
                confirmationMessageLines.Add("This file cannot be found in other directories.");

            string fullPath = targetDirectory.Directory.GetFullPath(RelativePath);
            string questionLine = string.Format("Are you sure you want to delete '{0}'?", fullPath);
            confirmationMessageLines.Add(questionLine);

            string message = string.Join("\n", confirmationMessageLines);
            return message;
        }

    }
}
