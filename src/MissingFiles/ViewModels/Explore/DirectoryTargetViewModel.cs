﻿using MissingFiles.Binding;
using MissingFiles.Infrastructure;
using MissingFiles.Models;

namespace MissingFiles.ViewModels.Explore
{
    internal class DirectoryTargetViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly Directory _directory;

        // ---- Init

        public DirectoryTargetViewModel(Directory directory)
        {
            Argument.ShouldNotBeNull(directory, "directory");

            _directory = directory;
        }

        // ---- Properties

        public Directory Directory
        {
            get { return _directory; }
        }

        public string Name
        {
            get { return _directory.Name; }
        }

    }
}
