﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Data;
using MissingFiles.Binding;
using MissingFiles.Collections;
using MissingFiles.Extensions;
using MissingFiles.Filters.Glob;
using MissingFiles.Infrastructure;
using MissingFiles.Models;

namespace MissingFiles.ViewModels.Explore
{
    internal class DirectoryBundleExploreViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly MissingFilesMainViewModel _mainViewModel;
        private readonly DirectoryBundle _directoryBundle;

        private readonly object _targetDirectoriesGuard = new object();
        private readonly ObservableCollection<DirectoryTargetViewModel> _targetDirectories =
            new ObservableCollection<DirectoryTargetViewModel>();
        private bool _targetDirectoriesLoaded = false;

        private readonly ObservableCollection<FileSystemEntryViewModel> _entries =
            new ObservableCollection<FileSystemEntryViewModel>();
        private readonly ICollectionView _entriesView;

        private readonly Command _refreshFilesCommand;

        private string _currentPath = string.Empty;
        private string _entriesFilter = string.Empty;
        private GlobFilter _globFilter = null;

        // ---- Init

        public DirectoryBundleExploreViewModel(MissingFilesMainViewModel mainViewModel, DirectoryBundle directoryBundle)
        {
            Argument.ShouldNotBeNull(mainViewModel, "mainViewModel");
            Argument.ShouldNotBeNull(directoryBundle, "directoryBundle");

            _mainViewModel = mainViewModel;
            _directoryBundle = directoryBundle;

            _entriesView = CollectionViewSource.GetDefaultView(_entries);
            _entriesView.Filter = FilterEntry;

            LoadFiles();

            _refreshFilesCommand = new Command(_ => LoadFiles(), null);
        }

        // ---- Properties

        public DirectoryBundle DirectoryBundle
        {
            get { return _directoryBundle; }
        }

        public ObservableCollection<DirectoryTargetViewModel> TargetDirectories
        {
            get { return _targetDirectories; }
        }

        public ObservableCollection<FileSystemEntryViewModel> Entries
        {
            get { return _entries; }
        }

        public ICollectionView EntriesView
        {
            get { return _entriesView; }
        }

        public string CurrentPath
        {
            get { return _currentPath; }
            private set
            {
                string newValue = value.AsPath();
                if (_currentPath == newValue)
                    return;

                _currentPath = newValue;

                ReportPropertyChanged("CurrentPath");
                LoadFiles();
            }
        }

        public string EntriesFilter
        {
            get { return _entriesFilter; }
            set
            {
                if (_entriesFilter == value)
                    return;

                _entriesFilter = value;

                ReportPropertyChanged("EntriesFilter");

                UpdateGlobFilter();
                _entriesView.Refresh();
            }
        }

        public Command RefreshFilesCommand
        {
            get { return _refreshFilesCommand; }
        }

        // ---- Operations

        public void DirectorySelected(DirectoryEntryViewModel directoryEntry)
        {
            UiThread.CheckCrossThreadAccess();

            if (directoryEntry != null)
                CurrentPath = directoryEntry.RelativePath;
            else
                CurrentPath = string.Empty;
        }

        public void ExecuteImmediateOperation(BackgroundOperation operation)
        {
            _mainViewModel.ExecuteImmediateOperation(operation);
        }

        private void LoadFiles()
        {
            UiThread.CheckCrossThreadAccess();

            var loadFilesOperation = new BackgroundOperation(LoadFilesAsynchronously, true);
            _mainViewModel.ExecuteImmediateOperation(loadFilesOperation);
        }

        private void LoadFilesAsynchronously(BackgroundOperation operation, CancellationToken cancelToken)
        {
            cancelToken.ThrowIfCancellationRequested();
            var targetDirectories = GetTargetDirectories();

            var result = _directoryBundle.GetMergedDirectoryContents(_currentPath, false, cancelToken);

            var fileSystemEntryViewModels =
                (from current in result.Entries
                 where (current != null)
                 let availableInDirectories =
                    targetDirectories.Where(currentTargetDirectory => current.AvailableInDirectories.Contains(currentTargetDirectory.Directory))
                 select CreateViewModel(current, availableInDirectories)).ToList();

            bool isRoot = string.IsNullOrEmpty(_currentPath);
            if (!isRoot)
                AddParentDirectoryReference(targetDirectories, fileSystemEntryViewModels);

            UiThread.Invoke(
                () =>
                {
                    operation.AddErrors(result.ErrorMessages);

                    using (_entriesView.DeferRefresh())
                    {
                        _entries.Clear();
                        _entries.AddRange(fileSystemEntryViewModels);
                    }
                });
        }

        private DirectoryTargetViewModel[] GetTargetDirectories()
        {
            DirectoryTargetViewModel[] targetDirectories;

            lock (_targetDirectoriesGuard)
            {
                if (_targetDirectoriesLoaded)
                    return _targetDirectories.ToArray();

                targetDirectories =
                    _directoryBundle.Directories.Select(current => new DirectoryTargetViewModel(current)).ToArray();
            }

            UiThread.Invoke(
                () =>
                {
                    lock (_targetDirectoriesGuard)
                    {
                        _targetDirectories.Clear();
                        _targetDirectories.AddRange(targetDirectories);

                        _targetDirectoriesLoaded = true;
                    }
                });

            return targetDirectories;
        }

        private FileSystemEntryViewModel CreateViewModel(FileSystemEntry entry, IEnumerable<DirectoryTargetViewModel> availableInDirectories)
        {
            Argument.ShouldNotBeNull(entry, "entry");

            if (entry is DirectoryEntry)
                return new DirectoryEntryViewModel(this, entry.RelativePath, false, availableInDirectories);

            if (entry is FileEntry)
                return new FileEntryViewModel(this, entry.RelativePath, availableInDirectories);

            return null;
        }

        private void AddParentDirectoryReference(DirectoryTargetViewModel[] targetDirectories, List<FileSystemEntryViewModel> fileSystemEntryViewModels)
        {
            string parentPath = System.IO.Path.GetDirectoryName(_currentPath);
            var upDirectoryEntry = new DirectoryEntryViewModel(this, parentPath, true, targetDirectories);

            if (fileSystemEntryViewModels.Count > 0)
                fileSystemEntryViewModels.Insert(0, upDirectoryEntry);
            else
                fileSystemEntryViewModels.Add(upDirectoryEntry);
        }

        private void UpdateGlobFilter()
        {
            const string FilterFormat = "*{0}*";

            if (string.IsNullOrEmpty(_entriesFilter))
                _globFilter = null;

            string filter = string.Format(FilterFormat, _entriesFilter);
            _globFilter = new GlobFilter(filter);
        }

        private bool FilterEntry(object item)
        {
            if ((_globFilter == null) || (!_globFilter.IsFiltering))
                return true;

            var directoryEntry = (item as DirectoryEntryViewModel);
            if ((directoryEntry != null) && (directoryEntry.IsParentReference))
                return true;

            var entry = (item as FileSystemEntryViewModel);
            if (entry == null)
                return false;

            bool passed = _globFilter.Passes(entry.Name);
            return passed;
        }

    }
}
