﻿using MissingFiles.Binding;
using MissingFiles.Configuration;
using MissingFiles.Infrastructure;

namespace MissingFiles.ViewModels.Bundle
{
    internal class DirectoryEditViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly DirectoryBundleEditViewModel _bundleEditViewModel;

        private bool _isNewDirectory;
        private readonly DirectoryConfiguration _configuration;

        private readonly Command _moveUpCommand;
        private readonly Command _moveDownCommand;
        private readonly Command _deleteCommand;

        // ---- Init

        public DirectoryEditViewModel(DirectoryBundleEditViewModel bundleEditViewModel, DirectoryConfiguration configuration, bool isNewDirectory)
        {
            Argument.ShouldNotBeNull(bundleEditViewModel, "bundleEditViewModel");
            Argument.Check((isNewDirectory) || (configuration != null), "configuration", "Configuration can only be null for a new bundle.");

            _bundleEditViewModel = bundleEditViewModel;
            _isNewDirectory = isNewDirectory;
            _configuration = (configuration ?? DirectoryConfiguration.CreateNew());

            _moveUpCommand = new Command(_ => MoveUp(), _ => CanMoveUp());
            _moveDownCommand = new Command(_ => MoveDown(), _ => CanMoveDown());
            _deleteCommand = new Command(_ => Delete(), _ => CanDelete());
        }

        // ---- Properties

        public string DisplayName
        {
            get { return _configuration.Name; }
            set
            {
                if (_configuration.Name == value)
                    return;

                _configuration.Name = value;

                ReportPropertyChanged("DisplayName");
                IsNewDirectory = false;
            }
        }

        public string Path
        {
            get { return _configuration.Path; }
            set
            {
                if (_configuration.Path == value)
                    return;

                _configuration.Path = value;

                ReportPropertyChanged("Path");
                IsNewDirectory = false;
            }
        }

        public bool IsNewDirectory
        {
            get { return _isNewDirectory; }
            private set
            {
                if (_isNewDirectory == value)
                    return;

                _isNewDirectory = value;

                ReportPropertyChanged("IsNewDirectory");
                DeleteCommand.ReportCanExecuteChanged();
                if (!_isNewDirectory)
                    _bundleEditViewModel.AddNewDirectoryElement();
            }
        }

        public Command MoveUpCommand
        {
            get { return _moveUpCommand; }
        }

        public Command MoveDownCommand
        {
            get { return _moveDownCommand; }
        }

        public Command DeleteCommand
        {
            get { return _deleteCommand; }
        }

        // ---- Operations

        public DirectoryConfiguration GetConfiguration()
        {
            return _configuration;
        }

        private bool CanMoveUp()
        {
            return _bundleEditViewModel.CanMoveUp(this);
        }

        private bool CanMoveDown()
        {
            return _bundleEditViewModel.CanMoveDown(this);
        }

        private void MoveUp()
        {
            _bundleEditViewModel.MoveUp(this);
        }

        private void MoveDown()
        {
            _bundleEditViewModel.MoveDown(this);
        }

        private bool CanDelete()
        {
            return (!IsNewDirectory);
        }

        private void Delete()
        {
            _bundleEditViewModel.Delete(this);
        }

    }
}
