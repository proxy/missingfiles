﻿using System.Linq;
using MissingFiles.Binding;
using MissingFiles.Collections;
using MissingFiles.Configuration;
using MissingFiles.Infrastructure;

namespace MissingFiles.ViewModels.Bundle
{
    internal class DirectoryBundleEditViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly DirectoryBundlesEditViewModel _bundlesEditViewModel;

        private bool _isNewBundle;
        private readonly DirectoryBundleConfiguration _configuration;

        private readonly ObservableCollection<DirectoryEditViewModel> _directories =
            new ObservableCollection<DirectoryEditViewModel>();

        private DirectoryEditViewModel _selectedDirectory;

        private readonly Command _moveUpCommand;
        private readonly Command _moveDownCommand;
        private readonly Command _deleteCommand;

        // ---- Init

        public DirectoryBundleEditViewModel(DirectoryBundlesEditViewModel bundlesEditViewModel, DirectoryBundleConfiguration configuration, bool isNewBundle)
        {
            Argument.ShouldNotBeNull(bundlesEditViewModel, "bundlesEditViewModel");
            Argument.Check((isNewBundle) || (configuration != null), "configuration", "Configuration can only be null for a new bundle.");

            _bundlesEditViewModel = bundlesEditViewModel;
            _isNewBundle = isNewBundle;
            _configuration = (configuration ?? DirectoryBundleConfiguration.CreateNew());

            var bundles = _configuration.Directories.Select(current => new DirectoryEditViewModel(this, current, false));
            _directories.AddRange(bundles);
            AddNewDirectoryElement();

            _moveUpCommand = new Command(_ => MoveUp(), _ => CanMoveUp());
            _moveDownCommand = new Command(_ => MoveDown(), _ => CanMoveDown());
            _deleteCommand = new Command(_ => Delete(), _ => CanDelete());
        }

        // ---- Properties

        public bool IsNewBundle
        {
            get { return _isNewBundle; }
            private set
            {
                if (_isNewBundle == value)
                    return;

                _isNewBundle = value;

                ReportPropertyChanged("IsNewBundle");
                DeleteCommand.ReportCanExecuteChanged();
                if (!_isNewBundle)
                    _bundlesEditViewModel.AddNewBundleElement();
            }
        }

        public string DisplayName
        {
            get { return _configuration.Name; }
            set
            {
                if (_configuration.Name == value)
                    return;

                _configuration.Name = value;

                ReportPropertyChanged("DisplayName");
                IsNewBundle = false;
            }
        }

        public Command MoveUpCommand
        {
            get { return _moveUpCommand; }
        }

        public Command MoveDownCommand
        {
            get { return _moveDownCommand; }
        }

        public Command DeleteCommand
        {
            get { return _deleteCommand; }
        }

        public ObservableCollection<DirectoryEditViewModel> Directories
        {
            get { return _directories; }
        }

        public DirectoryEditViewModel SelectedDirectory
        {
            get { return _selectedDirectory; }
            set
            {
                if (_selectedDirectory == value)
                    return;

                _selectedDirectory = value;

                ReportPropertyChanged("SelectedDirectory");
            }
        }

        // ---- Operations

        public DirectoryBundleConfiguration GetConfiguration()
        {
            _configuration.Directories =
                (from current in _directories
                 where (current != null) && (!current.IsNewDirectory)
                 select current.GetConfiguration()).ToList();

            return _configuration;
        }

        public void AddNewDirectoryElement()
        {
            var newBundle = new DirectoryEditViewModel(this, null, true);
            _directories.Add(newBundle);

            ReportMovingCommandsCanExecuteChanged();
        }

        public bool CanMoveUp(DirectoryEditViewModel directory)
        {
            if ((directory == null) || (directory.IsNewDirectory))
                return false;

            var firstBundle = _directories.FirstOrDefault();
            return (firstBundle != directory);
        }

        public bool CanMoveDown(DirectoryEditViewModel directory)
        {
            if ((directory == null) || (directory.IsNewDirectory))
                return false;

            var lastBundle = _directories.LastOrDefault(current => !current.IsNewDirectory);
            return (lastBundle != directory);
        }

        public void MoveUp(DirectoryEditViewModel directory)
        {
            bool isSelected = (SelectedDirectory == directory);

            bool moveSuccess = _directories.MoveItemUp(directory);
            if (!moveSuccess)
                return;

            if (isSelected)
                SelectedDirectory = directory;

            ReportMovingCommandsCanExecuteChanged();
        }

        public void MoveDown(DirectoryEditViewModel directory)
        {
            bool isSelected = (SelectedDirectory == directory);

            bool moveSuccess = _directories.MoveItemDown(directory);
            if (!moveSuccess)
                return;

            if (isSelected)
                SelectedDirectory = directory;

            ReportMovingCommandsCanExecuteChanged();
        }

        public void Delete(DirectoryEditViewModel directory)
        {
            _directories.Remove(directory);

            ReportMovingCommandsCanExecuteChanged();
        }

        private bool CanMoveUp()
        {
            return _bundlesEditViewModel.CanMoveUp(this);
        }

        private bool CanMoveDown()
        {
            return _bundlesEditViewModel.CanMoveDown(this);
        }

        private void MoveUp()
        {
            _bundlesEditViewModel.MoveUp(this);
        }

        private void MoveDown()
        {
            _bundlesEditViewModel.MoveDown(this);
        }

        private bool CanDelete()
        {
            return (!IsNewBundle);
        }

        private void Delete()
        {
            _bundlesEditViewModel.Delete(this);
        }

        private void ReportMovingCommandsCanExecuteChanged()
        {
            foreach (var current in _directories)
            {
                current.MoveUpCommand.ReportCanExecuteChanged();
                current.MoveDownCommand.ReportCanExecuteChanged();
            }
        }

    }
}
