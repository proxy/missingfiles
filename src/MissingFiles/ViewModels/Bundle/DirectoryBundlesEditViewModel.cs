﻿using System.Linq;
using MissingFiles.Binding;
using MissingFiles.Collections;
using MissingFiles.Configuration;
using MissingFiles.Infrastructure;

namespace MissingFiles.ViewModels.Bundle
{
    internal class DirectoryBundlesEditViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly MissingFilesMainViewModel _mainViewModel;
        private readonly DirectoryBundlesConfiguration _configuration;

        private readonly ObservableCollection<DirectoryBundleEditViewModel> _bundles =
            new ObservableCollection<DirectoryBundleEditViewModel>();

        private DirectoryBundleEditViewModel _selectedBundle;

        private readonly Command _saveCommand;
        private readonly Command _cancelCommand;

        // ---- Init

        public DirectoryBundlesEditViewModel(MissingFilesMainViewModel mainViewModel, DirectoryBundlesConfiguration configuration)
        {
            Argument.ShouldNotBeNull(mainViewModel, "mainViewModel");
            Argument.ShouldNotBeNull(configuration, "configuration");

            _mainViewModel = mainViewModel;
            _configuration = configuration;

            var bundles = _configuration.Items.Select(current => new DirectoryBundleEditViewModel(this, current, false));
            _bundles.AddRange(bundles);

            AddNewBundleElement();
            _selectedBundle = _bundles[0];

            _saveCommand = new Command(_ => Save(), null);
            _cancelCommand = new Command(_ => Cancel(), null);
        }

        // ---- Properties

        public ObservableCollection<DirectoryBundleEditViewModel> Bundles
        {
            get { return _bundles; }
        }

        public DirectoryBundleEditViewModel SelectedBundle
        {
            get { return _selectedBundle; }
            set
            {
                if (_selectedBundle == value)
                    return;

                _selectedBundle = value;

                ReportPropertyChanged("SelectedBundle");
            }
        }

        public Command SaveCommand
        {
            get { return _saveCommand; }
        }

        public Command CancelCommand
        {
            get { return _cancelCommand; }
        }

        // ---- Operations

        public void AddNewBundleElement()
        {
            var newBundle = new DirectoryBundleEditViewModel(this, null, true);
            _bundles.Add(newBundle);

            ReportMovingCommandsCanExecuteChanged();
        }

        public bool CanMoveUp(DirectoryBundleEditViewModel directoryBundle)
        {
            if ((directoryBundle == null) || (directoryBundle.IsNewBundle))
                return false;

            var firstBundle = _bundles.FirstOrDefault();
            return (firstBundle != directoryBundle);
        }

        public bool CanMoveDown(DirectoryBundleEditViewModel directoryBundle)
        {
            if ((directoryBundle == null) || (directoryBundle.IsNewBundle))
                return false;

            var lastBundle = _bundles.LastOrDefault(current => !current.IsNewBundle);
            return (lastBundle != directoryBundle);
        }

        public void MoveUp(DirectoryBundleEditViewModel directoryBundle)
        {
            bool isSelected = (SelectedBundle == directoryBundle);

            bool moveSuccess = _bundles.MoveItemUp(directoryBundle);
            if (!moveSuccess)
                return;

            if (isSelected)
                SelectedBundle = directoryBundle;

            ReportMovingCommandsCanExecuteChanged();
        }

        public void MoveDown(DirectoryBundleEditViewModel directoryBundle)
        {
            bool isSelected = (SelectedBundle == directoryBundle);

            bool moveSuccess = _bundles.MoveItemDown(directoryBundle);
            if (!moveSuccess)
                return;

            if (isSelected)
                SelectedBundle = directoryBundle;

            ReportMovingCommandsCanExecuteChanged();
        }

        public void Delete(DirectoryBundleEditViewModel directoryBundle)
        {
            _bundles.Remove(directoryBundle);

            ReportMovingCommandsCanExecuteChanged();
        }

        private void ReportMovingCommandsCanExecuteChanged()
        {
            foreach (var current in _bundles)
            {
                current.MoveUpCommand.ReportCanExecuteChanged();
                current.MoveDownCommand.ReportCanExecuteChanged();
            }
        }

        private void UpdateConfiguration()
        {
            _configuration.Items =
                (from current in _bundles
                 where (current != null) && (!current.IsNewBundle)
                 select current.GetConfiguration()).ToList();
        }

        private void Save()
        {
            UpdateConfiguration();

            _mainViewModel.SaveBundles();
        }

        private void Cancel()
        {
            _mainViewModel.CloseBundlesEdit();
        }

    }
}
