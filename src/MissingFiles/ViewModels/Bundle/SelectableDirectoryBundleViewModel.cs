﻿using MissingFiles.Infrastructure;
using MissingFiles.Models;

namespace MissingFiles.ViewModels.Bundle
{
    internal class SelectableDirectoryBundleViewModel
    {

        // ---- Fields

        private readonly DirectoryBundle _directoryBundle;
        private readonly bool _isSettingsItem;

        // ---- Init

        public SelectableDirectoryBundleViewModel(DirectoryBundle directoryBundle, bool isSettingsItem)
        {
            Argument.Check((isSettingsItem) || (directoryBundle != null), "directoryBundle", "DirectoryBundle cannot be null!");

            _directoryBundle = directoryBundle;
            _isSettingsItem = isSettingsItem;
        }

        // ---- Properties

        public DirectoryBundle DirectoryBundle
        {
            get { return _directoryBundle; }
        }

        public string DisplayName
        {
            get
            {
                if (_isSettingsItem)
                    return "Bundles...";

                return _directoryBundle.Name;
            }
        }

        public bool IsSettingsItem
        {
            get { return _isSettingsItem; }
        }

    }
}
