﻿using System.Reflection;
using MissingFiles.Binding;
using MissingFiles.Infrastructure;
using MissingFiles.Properties;

namespace MissingFiles.ViewModels
{
    internal class AboutViewModel
    {

        // ---- Fields

        private readonly MissingFilesMainViewModel _mainViewModel;

        private readonly string _aboutText;
        private readonly string _licenseText;

        private readonly Command _okCommand;

        // ---- Init

        public AboutViewModel(MissingFilesMainViewModel mainViewModel)
        {
            Argument.ShouldNotBeNull(mainViewModel, "mainViewModel");

            _mainViewModel = mainViewModel;

            _aboutText = GetAboutText();
            _licenseText = Resources.License;
            _okCommand = new Command(_ => Close(), null);
        }

        // ---- Properties

        public string AboutText
        {
            get { return _aboutText; }
        }

        public string LicenseText
        {
            get { return _licenseText; }
        }

        public Command OkCommand
        {
            get { return _okCommand; }
        }

        // ---- Operations

        private static string GetAboutText()
        {
            const string AboutTextFormat = "MissingFiles (version {0})";

            var assembly = Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName();
            string versionString = assemblyName.Version.ToString();

            string aboutText = string.Format(AboutTextFormat, versionString);
            return aboutText;
        }

        private void Close()
        {
            _mainViewModel.CloseLicenseView();
        }

    }
}
