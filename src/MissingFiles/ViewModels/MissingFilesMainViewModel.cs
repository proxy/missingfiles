﻿using System.Linq;
using MissingFiles.Binding;
using MissingFiles.Collections;
using MissingFiles.Configuration;
using MissingFiles.Infrastructure;
using MissingFiles.Models;
using MissingFiles.ViewModels.Bundle;
using MissingFiles.ViewModels.Explore;

namespace MissingFiles.ViewModels
{
    internal class MissingFilesMainViewModel : NotifyPropertyChanged
    {

        // ---- Fields

        private readonly DirectoryBundlesConfiguration _configuration;

        private readonly ObservableCollection<SelectableDirectoryBundleViewModel> _bundles =
            new ObservableCollection<SelectableDirectoryBundleViewModel>();
        private SelectableDirectoryBundleViewModel _selectedBundle;
        private SelectableDirectoryBundleViewModel _previousSelectedBundle;

        private readonly BackgroundOperationsAggregator _backgroundOperations = new BackgroundOperationsAggregator();

        private DirectoryBundleExploreViewModel _currentExploreViewModel;
        private DirectoryBundlesEditViewModel _currentEditViewModel;
        private AboutViewModel _aboutViewModel;

        private readonly Command _showAboutCommand;

        // ---- Init

        public MissingFilesMainViewModel()
        {
            _configuration = DirectoryBundlesConfiguration.Load();
            
            LoadDirectoryBundles();

            _showAboutCommand = new Command(_ => ShowAbout(), null);
        }

        // ---- Properties

        public ObservableCollection<SelectableDirectoryBundleViewModel> Bundles
        {
            get { return _bundles; }
        }

        public SelectableDirectoryBundleViewModel SelectedBundle
        {
            get { return _selectedBundle; }
            set
            {
                if (_selectedBundle == value)
                    return;

                if ((value != null) && (value.IsSettingsItem))
                {
                    _previousSelectedBundle = _selectedBundle;
                    CurrentEditViewModel = new DirectoryBundlesEditViewModel(this, _configuration);
                }

                _selectedBundle = value;

                ReportPropertyChanged("SelectedBundle");

                if ((_selectedBundle != null) && (!_selectedBundle.IsSettingsItem))
                    CurrentExploreViewModel = new DirectoryBundleExploreViewModel(this, _selectedBundle.DirectoryBundle);
            }
        }

        public BackgroundOperationsAggregator BackgroundOperations
        {
            get { return _backgroundOperations; }
        }

        public DirectoryBundleExploreViewModel CurrentExploreViewModel
        {
            get { return _currentExploreViewModel; }
            private set
            {
                if (_currentExploreViewModel == value)
                    return;

                _currentExploreViewModel = value;

                ReportPropertyChanged("CurrentExploreViewModel");
            }
        }

        public DirectoryBundlesEditViewModel CurrentEditViewModel
        {
            get { return _currentEditViewModel; }
            private set
            {
                if (_currentEditViewModel == value)
                    return;

                _currentEditViewModel = value;

                ReportPropertyChanged("CurrentEditViewModel");
            }
        }

        public AboutViewModel AboutViewModel
        {
            get { return _aboutViewModel; }
            private set
            {
                if (_aboutViewModel == value)
                    return;

                _aboutViewModel = value;

                ReportPropertyChanged("AboutViewModel");
            }
        }

        public Command ShowAboutCommand
        {
            get { return _showAboutCommand; }
        }

        // ---- Operations

        public void ExecuteImmediateOperation(BackgroundOperation operation)
        {
            _backgroundOperations.Add(operation);

            operation.FinishedAndClear += (sender, eventArgs) => _backgroundOperations.Remove(operation);
            operation.Execute();
        }

        public void SaveBundles()
        {
            CurrentEditViewModel = null;

            var backgroundSave =
                new BackgroundOperation(
                    (operation, CancelToken) =>
                    {
                        _configuration.Save();

                        UiThread.Invoke(() => LoadDirectoryBundles());

                    }, false);

            ExecuteImmediateOperation(backgroundSave);
        }

        public void CloseBundlesEdit()
        {
            CurrentEditViewModel = null;
            SelectedBundle = _previousSelectedBundle;
            _previousSelectedBundle = null;
        }

        public void CloseLicenseView()
        {
            AboutViewModel = null;
        }

        private void LoadDirectoryBundles()
        {
            _bundles.Clear();

            var selectableBundles =
                (from current in _configuration.Items
                 where (current != null) && (current.Name != null)
                 let currentBundle = DirectoryBundle.FromConfiguration(current)
                 select new SelectableDirectoryBundleViewModel(currentBundle, false));

            _bundles.AddRange(selectableBundles);

            var settingsItem = new SelectableDirectoryBundleViewModel(null, true);
            _bundles.Add(settingsItem);

            if (_previousSelectedBundle != null)
            {
                SelectedBundle = _bundles.FirstOrDefault(current => current.DisplayName == _previousSelectedBundle.DisplayName);
                _previousSelectedBundle = null;
            }

            if ((SelectedBundle == null) && (_bundles.Count > 0))
                SelectedBundle = _bundles[0];
        }

        private void ShowAbout()
        {
            AboutViewModel = new AboutViewModel(this);
        }

    }
}
